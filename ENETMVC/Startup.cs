﻿using Microsoft.Owin;
using Owin;
using System;
using System.IO;

[assembly: OwinStartupAttribute(typeof(ENETMVC.Startup))]
namespace ENETMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // set path to database file
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\ENETMVC.Database\Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
            ConfigureAuth(app);
        }
    }
}
