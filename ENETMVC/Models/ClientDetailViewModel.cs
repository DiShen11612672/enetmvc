﻿using ENETMVC.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENETMVC.Models
{
    public class ClientDetailViewModel
    {
        public Client Client { get; set; }
        public List<DataModel.Intervention> Interventions { get; set; }
    }
}