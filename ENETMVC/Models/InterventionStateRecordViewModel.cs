﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ENETMVC.Models
{
    public class InterventionStateRecordViewModel
    {
        [DisplayName("State")]
        public SelectList PossibleStates { get; set; }
        public string Employee { get; set; }
        [DisplayName("Creation Date")]
        public DateTime CreationDate { get; set; }

        [Required]
        public string SelectedState { get; set; }
    }
}