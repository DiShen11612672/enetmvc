﻿
using ENETMVC.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENETMVC.Models
{
    public class InterventionReviewViewModel
    {
        public Intervention Intervention { get; set; }
    }
}