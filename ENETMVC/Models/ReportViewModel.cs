﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ENETMVC.Models
{
    public class ReportViewModel
    {
        public IEnumerable<dynamic> Report { get; set; }

        public IEnumerable<dynamic> GrandReport { get; set; }
      
    }
}