﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using ENETMVC.Database.Models;

namespace ENETMVC.Models
{
    public class SiteEngineerIndexViewModel
    {

        public List<Client> DistrictClients { get; set; }
    }
}