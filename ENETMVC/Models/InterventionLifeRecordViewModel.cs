﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ENETMVC.Models
{
    public class InterventionLifeRecordViewModel
    {
        [Required]
        [Range(0, 100, ErrorMessage = "Remaining Life has to be between 0 and 100.")]
        [DisplayName("Remaining Life")]
        public decimal RemainingLife { get; set; }
        public string Employee { get; set; }
        [DisplayName("Date of Visit")]
        public DateTime DateOfVisit { get; set; }
        [Required]
        public string Notes { get; set; }
    }
}