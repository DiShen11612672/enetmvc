﻿using ENETMVC.Database.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ENETMVC.Models
{
    public class NewInterventionViewModel
    {

        public DateTime CurrentDate { get; set; }
        public string ClientName { get; set; }
        public SelectList InterventionType { get; set; }
        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Don't be naughty")]
        public decimal Cost { get; set; }
        [Required]
        [Range(0, double.MaxValue, ErrorMessage = "Don't be naughty")]
        public decimal Duration { get; set; }
        public string SiteEngineer { get; set; }
        [Required]
        public int SelectedValue { get; set; }
    }
}