﻿using ENETMVC.Database;
using ENETMVC.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENETMVC.Models
{
    public class EmployeeViewModel
    {
        public ApprovableEmployee Employee { get; set; }
    }
}