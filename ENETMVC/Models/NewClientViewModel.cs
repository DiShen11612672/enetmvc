﻿using ENETMVC.Database.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ENETMVC.Models
{
    public class NewClientViewModel
    {
        [Required(ErrorMessage = "Please fill out the form")]
        public Client Client { get; set; }
    }
}