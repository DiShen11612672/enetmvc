﻿using ENETMVC.Database.Models;
using System.Collections.Generic;

namespace ENETMVC.Models
{
    public class AccountantViewModel
    {
        public List<SiteEngineer> SiteEngineers { get; set; }
        public List<Manager> Managers { get; set; }
    }
}