﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ENETMVC.Database.Models;

namespace ENETMVC.Models
{
    public class ManagerViewModel
    {
        // List of proposed interventions to be approved.
        [Required]
        public List<Intervention> ProposedInterventions
        {
            get;
            private set;
        }

        // List of past interventions not yet cancelled.
        [Required]
        public List<Intervention> PastInterventions
        {
            get;
            private set;
        }

        /// <summary>
        /// Default constructor.
        /// Creates lists to store proposed interventions and
        /// interventions approved by a particular manager.
        /// </summary>
        public ManagerViewModel()
        {
            ProposedInterventions = new List<Intervention>();
            PastInterventions = new List<Intervention>();
        }

    }
}