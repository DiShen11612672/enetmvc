﻿
using ENETMVC.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENETMVC.Models
{
    public class InterventionViewModel
    {
        public List<Intervention> Interventions { get; set; }
    }
}