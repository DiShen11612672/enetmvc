﻿using ENETMVC.Database;
using ENETMVC.Database.Interfaces;
using ENETMVC.Database.Models;
using ENETMVC.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using System.Linq;

namespace ENETMVC.Controllers
{
    public class InterventionController : Controller
    {
        private IDatabaseFacadeFactory dbFacadeFactory;

        public InterventionController() : this(new DatabaseFacadeFactory())
        {
        }

        public InterventionController(IDatabaseFacadeFactory dbFacadeFactory)
        {
            if (dbFacadeFactory == null)
            {
                throw new ArgumentNullException();
            }
            this.dbFacadeFactory = dbFacadeFactory;
        }

        private void AddInterventionStateRecord(IDatabaseFacade databaseFacade, Employee employee, Intervention intervention, string interventionState)
        {
            if ((employee != null) && (employee is ApprovableEmployee) && (intervention != null))
            {
                var stateRecord = new InterventionStateRecord()
                {
                    InterventionId = intervention.Id,
                    EmployeeId = employee.Id,
                    Created = DateTime.Now
                };

                var approvableEmployee = (ApprovableEmployee)employee;
                switch (interventionState)
                {
                    case "APPROVE":
                        intervention.Approve(approvableEmployee);
                        stateRecord.State = State.APPROVE;
                        break;
                    case "COMPLETE":
                        intervention.Complete(approvableEmployee);
                        stateRecord.State = State.COMPLETE;
                        break;
                    case "CANCEL":
                        intervention.Cancel(approvableEmployee);
                        stateRecord.State = State.CANCEL;
                        break;
                    default:
                        throw new InvalidEnumArgumentException();
                }

                try
                {
                    databaseFacade.BeginTransaction();
                    databaseFacade.SaveOrUpdate(intervention);
                    databaseFacade.SaveOrUpdate(stateRecord);
                    databaseFacade.CommitTransaction();
                }
                catch (Exception)
                {
                    databaseFacade.RollbackTransaction();
                    throw;
                }
            }
        }

        private void AddInterventionLifeRecord(IDatabaseFacade databaseFacade, Employee employee, Intervention intervention, decimal remainingLife, string notes)
        {
            if ((employee != null) && (intervention != null))
            {
                var lifeRecord = new InterventionLifeRecord()
                {
                    InterventionId = intervention.Id,
                    EmployeeId = employee.Id,
                    RemainingLife = remainingLife,
                    Notes = notes,
                    DateOfVisit = DateTime.Now
                };

                try
                {
                    databaseFacade.BeginTransaction();
                    databaseFacade.SaveOrUpdate(lifeRecord);
                    databaseFacade.CommitTransaction();
                }
                catch (Exception)
                {
                    databaseFacade.RollbackTransaction();
                    throw;
                }
            }
        }

        [HttpGet]
        [Authorize(Roles = "SiteEngineer")]
        [Route("Intervention/{interventionId:int}/LifeRecord")]
        public ActionResult InterventionLifeRecord(int interventionId)
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                // initialize intervention controller
                var employee = databaseFacade.GetEmployeeByUserId(User.Identity.GetUserId());
                var intervention = databaseFacade.GetById<Intervention>(interventionId);

                if ((employee != null) && (intervention != null))
                {
                    // initialize view model
                    var viewModel = new InterventionLifeRecordViewModel()
                    {
                        Employee = employee.Name,
                        DateOfVisit = DateTime.Now
                    };
                    return View(viewModel);
                }
                return new HttpNotFoundResult();
            }
        }

        [HttpPost]
        [Authorize(Roles = "SiteEngineer")]
        [Route("Intervention/{interventionId:int}/LifeRecord")]
        public ActionResult InterventionLifeRecord(int interventionId, InterventionLifeRecordViewModel viewModel)
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                // initialize intervention controller
                var employee = databaseFacade.GetEmployeeByUserId(User.Identity.GetUserId());
                var intervention = databaseFacade.GetById<Intervention>(interventionId);

                if ((employee != null) && (intervention != null))
                {
                    if (ModelState.IsValid)
                    {
                        try
                        {
                            // add intervention life record
                            AddInterventionLifeRecord(databaseFacade, employee, intervention, viewModel.RemainingLife, viewModel.Notes);
                            return RedirectToAction("InterventionReview", "Intervention");
                        }
                        catch (Exception)
                        {
                            ModelState.AddModelError(string.Empty, "Failed to create intervention life record.");
                        }
                    }
                    return View(viewModel);
                }
                return new HttpNotFoundResult();
            }
        }

        [HttpGet]
        [Authorize(Roles = "SiteEngineer")]
        [Route("Intervention/{interventionId:int}/StateRecord")]
        public ActionResult InterventionStateRecord(int interventionId)
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                // initialize intervention controller
                var employee = databaseFacade.GetEmployeeByUserId(User.Identity.GetUserId());
                var intervention = databaseFacade.GetById<Intervention>(interventionId);

                if ((employee != null) && (intervention != null))
                {
                    // initialize view model
                    var viewModel = new InterventionStateRecordViewModel()
                    {
                        PossibleStates = new SelectList(intervention.GetPossibleInterventionStates()),
                        Employee = employee.Name,
                        CreationDate = DateTime.Now
                    };
                    return View(viewModel);
                }
                return new HttpNotFoundResult();
            }
        }

        [HttpPost]
        [Authorize(Roles = "SiteEngineer,Manager")]
        [Route("Intervention/{interventionId:int}/StateRecord")]
        public ActionResult InterventionStateRecord(int interventionId, InterventionStateRecordViewModel viewModel)
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                // initialize intervention controller
                var employee = databaseFacade.GetEmployeeByUserId(User.Identity.GetUserId());
                var intervention = databaseFacade.GetById<Intervention>(interventionId);

                if ((employee != null) && (intervention != null))
                {
                    if (ModelState.IsValid)
                    {
                        try
                        {
                            // add intervention state record
                            AddInterventionStateRecord(databaseFacade, employee, intervention, viewModel.SelectedState);
                            if (User.IsInRole("SiteEngineer"))
                            {
                                return RedirectToAction("InterventionReview", "Intervention");
                            }
                            else
                            {
                                return RedirectToAction("ProposedInterventions", "Intervention");
                            }
                        }
                        catch (Exception)
                        {
                            ModelState.AddModelError(string.Empty, "Failed to change intervention state! Either the costs or the duration of the intervention exceeds your quota.");
                        }
                    }
                    viewModel.PossibleStates = new SelectList(intervention.GetPossibleInterventionStates());
                    if (User.IsInRole("SiteEngineer"))
                    {
                        return View(viewModel);
                    }
                    else
                    {
                        return RedirectToAction("ProposedInterventions", "Intervention");
                    }
                }
                return new HttpNotFoundResult();
            }
        }

        private List<DataModel.Intervention> GetMyInterventions(IDatabaseFacade databaseFacade, int id)
        {
            List<DataModel.Intervention> interventions = new List<DataModel.Intervention>();
            var temp = databaseFacade.GetInterventionsByEmployeeId(id).Where(i => i.State != State.CANCEL);
            foreach (var item in temp)
            {
                DataModel.Intervention intervention = new DataModel.Intervention()
                {
                    Id = item.Id,
                    Employee = item.Employee,
                    Client = item.Client,
                    Costs = item.Costs,
                    Duration = item.Duration,
                    InterventionType = item.InterventionType,
                    Created = item.Created
                };
                interventions.Add(intervention);
            }
            return interventions;
        }

        //list all interventions of a site engineer
        [HttpGet]
        [Authorize(Roles = "SiteEngineer")]
        public ActionResult Intervention()
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                // initialize intervention controller
                var employee = databaseFacade.GetEmployeeByUserId(User.Identity.GetUserId());

                if (employee != null)
                {
                    // initialize view model
                    var viewModel = new InterventionViewModel()
                    {
                        Interventions = GetMyInterventions(databaseFacade, employee.Id)
                    };
                    return View(viewModel);
                }
                return new HttpNotFoundResult();
            }
        }

        //return a intervention of a client with local variable
        private DataModel.Intervention GetIntervention(IDatabaseFacade databaseFacade, int id)
        {
            Intervention temp = databaseFacade.GetById<Intervention>(id);
            DataModel.Intervention intervention = new DataModel.Intervention()
            {
                Employee = temp.Employee,
                Client = temp.Client,
                Costs = temp.Costs,
                Created = temp.Created,
                InterventionType = temp.InterventionType,
                Duration = temp.Duration,
                Id = temp.Id,
                StateRecords = GetStateRecords(databaseFacade, id),
                LifeRecords = GetLifeRecords(databaseFacade, id)
            };
            return intervention;
        }

        //return a list of interventions of a client with local variable
        private List<DataModel.InterventionLifeRecord> GetLifeRecords(IDatabaseFacade databaseFacade, int id)
        {
            List<DataModel.InterventionLifeRecord> lifeRecords = new List<DataModel.InterventionLifeRecord>();
            List<InterventionLifeRecord> temp = databaseFacade.GetInterventionLifeRecordsByInterventionId(id);
            foreach (var item in temp)
            {
                DataModel.InterventionLifeRecord lifeRecord = new DataModel.InterventionLifeRecord()
                {
                    DateOfVisit = item.DateOfVisit,
                    Employee = item.Employee.Name,
                    Notes = item.Notes,
                    RemainingLife = item.RemainingLife
                };
                lifeRecords.Add(lifeRecord);
            }
            return lifeRecords;
        }

        //return a list of intervention states of a client with local variable
        private List<DataModel.InterventionState> GetStateRecords(IDatabaseFacade databaseFacade, int id)
        {
            List<DataModel.InterventionState> stateRecords = new List<DataModel.InterventionState>();
            List<InterventionStateRecord> temp = databaseFacade.GetInterventionStateRecordsByInterventionId(id);
            foreach (var item in temp)
            {
                DataModel.InterventionState stateRecord = new DataModel.InterventionState()
                {
                    Employee = item.Employee.Name,
                    Created = item.Created,
                    State = item.StateString
                };
                stateRecords.Add(stateRecord);
            }
            return stateRecords;
        }

        //for site engineer interventionreview page
        //return the intervention details, state records, and life records
        [HttpGet]
        [Authorize(Roles = "SiteEngineer")]
        [Route("Intervention/{interventionId:int}/InterventionReview")]
        public ActionResult InterventionReview(int interventionId)
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                // initialize intervention controller
                var employee = databaseFacade.GetEmployeeByUserId(User.Identity.GetUserId());
                var intervention = databaseFacade.GetById<Intervention>(interventionId);
                if (employee != null && intervention != null)
                {
                    // initialize view model
                    var viewModel = new InterventionReviewViewModel()
                    {
                        Intervention = GetIntervention(databaseFacade, interventionId)
                    };

                    return View(viewModel);
                }
                return new HttpNotFoundResult();
            }
        }

        /// <summary>
        /// Gets a list of all interventions from database and
        /// returns a subset of interventions that belong to the same district
        /// as the site engineer/manager and have a state of "PROPOSE".
        /// </summary>
        /// <param name="databaseFacade">Database facade to use when querying the database.</param>
        /// <param name="manager">Manager retrieving proposed interventions.</param>
        /// <returns>List of proposed interventions</returns>
        private List<Intervention> LoadProposedInterventions(IDatabaseFacade databaseFacade, Manager manager)
        {
            if (databaseFacade == null)
            {
                var exception = new Exception("No database facade provided!");
                throw exception;
            }

            var propInters = from i in LoadAllInterventions(databaseFacade)
                             where i.Client.District == manager.District
                             where i.State == State.PROPOSE
                             select i;

            return propInters.ToList();
        }

        /// <summary>
        /// Gets a list of all interventions from database and
        /// returns a subset of interventions that have an intervention state record
        /// added by the manager and don't have a state of "CANCEL".
        /// </summary>
        /// <param name="databaseFacade">Database facade to use when querying the database.</param>
        /// <param name="manager">Manager retrieving past interventions</param>
        private List<Intervention> LoadPastInterventions(IDatabaseFacade databaseFacade, Manager manager)
        {
            if (databaseFacade == null)
            {
                var exception = new Exception("No database facade provided!");
                throw exception;
            }

            var pastInters = from i in LoadAllInterventions(databaseFacade)
                             where i.InterventionStateRecords.Count(isr => isr.EmployeeId == manager.Id) > 0
                             select i;

            return pastInters.ToList();
        }

        /// <summary>
        /// Retrieve all interventions from the database and add to the private allInters list.
        /// </summary>
        /// <param name="databaseFacade">Database facade to use when querying the database.</param>
        /// <returns>List of all interventions found in the database.</returns>
        private List<Intervention> LoadAllInterventions(IDatabaseFacade databaseFacade)
        {
            if (databaseFacade == null)
            {
                var exception = new Exception("No database facade provided!");
                throw exception;
            }

            var interventions = databaseFacade.GetAll<Intervention>();

            // Create full objects for each intervention attribute.
            foreach (var i in interventions)
            {
                var employee = i.Employee;
                var client = i.Client;
                var interventionType = i.InterventionType;
                var interventionStateRecords = i.InterventionStateRecords;
                var interventionLifeRecords = i.InterventionLifeRecords;
            }

            return interventions;
        }

        /// <summary>
        /// Retrieve manager details from database.
        /// </summary>
        /// <param name="databaseFacade">Database facade to use to access the DB.</param>
        /// <param name="id">User ID for manager user.</param>
        /// <returns>Manager details retrieved from database.</returns>
        private Manager LoadEmployeeById(IDatabaseFacade databaseFacade, string id)
        {
            if (databaseFacade == null)
            {
                var exception = new Exception("No database facade provided!");
                throw exception;
            }

            if (string.IsNullOrWhiteSpace(id))
            {
                var exception = new Exception("No employee ID provided!");
                throw exception;
            }

            var manager = (Manager)databaseFacade.GetEmployeeByUserId(id);

            return manager;
        }

        /// <summary>
        /// Page listing proposed interventions for approval.
        /// </summary>
        /// <returns>ProposedInterventions view page.</returns>
        [HttpGet]
        [Authorize(Roles = "Manager")]
        public ActionResult ProposedInterventions()
        {
            // Title and heading for ProposedInterventions page.
            ViewBag.TitleHead = "Proposed Interventions";

            // Manager retrieving proposed interventions.
            Manager manager;

            // Connect to database via facade and load user data
            // and proposed interventions into the manager object.
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                try
                {
                    manager = LoadEmployeeById(databaseFacade, User.Identity.GetUserId());
                    manager.ProposedInterventions.AddRange(LoadProposedInterventions(databaseFacade, manager));
                }
                catch (Exception e)
                {
                    // Return 404 Not Found page with message indicating what went wrong.
                    return new HttpNotFoundResult(e.Message);
                }
            }

            // Create view model and add proposed interventions list.
            var manViewMod = new ManagerViewModel();
            manViewMod.ProposedInterventions.AddRange(manager.ProposedInterventions);

            // Parse view model to view page.
            return View(manViewMod);
        }

        /// <summary>
        /// Page lists interventions approved by a particular manager.
        /// </summary>
        /// <returns>Past Interventions view page.</returns>
        [HttpGet]
        [Authorize(Roles = "Manager")]
        public ActionResult PastInterventions()
        {
            // Title and heading for PastInterventions page.
            ViewBag.TitleHead = "Past Interventions";

            // Manager retrieving proposed interventions.
            Manager manager;

            // Connect to database via facade and load user data
            // and past interventions into the manager object.
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                try
                {
                    manager = LoadEmployeeById(databaseFacade, User.Identity.GetUserId());
                    manager.PastInterventions.AddRange(LoadPastInterventions(databaseFacade, manager));
                }
                catch (Exception e)
                {
                    // Return 404 Not Found page with message indicating what went wrong.
                    return new HttpNotFoundResult(e.Message);
                }
            }

            // Create view model and add proposed interventions list.
            var manViewMod = new ManagerViewModel();
            manViewMod.PastInterventions.AddRange(manager.PastInterventions);

            // Parse view model to view page.
            return View(manViewMod);
        }
    }
}