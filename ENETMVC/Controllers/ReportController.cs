﻿using ENETMVC.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using ENETMVC.Models;
namespace ENETMVC.Controllers
{
    [Authorize(Roles = "Accountant")]
    public class ReportController : Controller
    {
        public ActionResult DistrictMonthlyCostReport()
        {
            ViewBag.District = getItems();

            return View();

        }

        [HttpPost]
        public ActionResult DistrictMonthlyCostReport(District District)
        {
            ViewBag.District = getItems();
            ViewBag.messageString = District.ToString();
            String query = "select Name, sum(Costs), sum(Duration) from intervention right outer join Months on Months.id = DATEPart(mm, Created) AND clientId in (select id from Client where District = '" + District.ToString() + "')   AND state = 'COMPLETE' group by Name, Months.Id order by Months.Id;";
            return View(getViewModel(query));
        }

        public ReportViewModel getViewModel(string query)
        {

            var viewModel = new ReportViewModel
            {
                Report = getReport(query),
                GrandReport = getReport("select sum(Intervention.Duration), sum(Intervention.Costs) from Intervention where State = 'COMPLETE'"),

            };
            return viewModel;
        }

        public List<SelectListItem> getItems()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            items.Add(new SelectListItem { Text = District.RURAL_INDONESIA.ToString(), Value = "0" });
            items.Add(new SelectListItem { Text = District.RURAL_NSW.ToString(), Value = "1" });
            items.Add(new SelectListItem { Text = District.RURAL_PNG.ToString(), Value = "2" });
            items.Add(new SelectListItem { Text = District.SYDNEY.ToString(), Value = "3" });
            items.Add(new SelectListItem { Text = District.URBAN_INDONESIA.ToString(), Value = "4" });
            items.Add(new SelectListItem { Text = District.URBAN_PNG.ToString(), Value = "5" });
            return items;

        }

        public ActionResult DistrictCostReport()
        {
            String query = "select District, sum(Intervention.Duration), sum(Intervention.Costs) from Client left outer join Intervention on Client.Id = Intervention.ClientId AND state = 'COMPLETE' group by District";
            return View(getViewModel(query));
        }

        public ActionResult SiteEngineerCostReport()
        {
            String query = "select Employee.Name, sum(Intervention.Duration),sum(Intervention.Costs), avg(Intervention.Duration), avg(Intervention.Costs) from Intervention right outer join  Employee on state = 'COMPLETE' AND Employee.Id = EmployeeId group by Employee.Name order by Employee.Name";
            return View(getViewModel(query));
        }

        public IEnumerable<dynamic> getReport(string s)
        {
            var db = WebMatrix.Data.Database.Open("DefaultConnection");
            return db.Query(s);
        }

        public ActionResult Index()
        {
            return View();
        }

    }
}
