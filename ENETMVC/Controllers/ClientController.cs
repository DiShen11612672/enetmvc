﻿using ENETMVC.Database.Models;
using ENETMVC.Database;
using ENETMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ENETMVC.Database.Interfaces;

namespace ENETMVC.Controllers
{
    [Authorize(Roles = "SiteEngineer")]
    public class ClientController : Controller
    {
        private IDatabaseFacadeFactory dbFacadeFactory;
        Client client;

        public ClientController() : this(new DatabaseFacadeFactory())
        {
        }

        public ClientController(IDatabaseFacadeFactory dbFacadeFactory)
        {
            if (dbFacadeFactory == null)
            {
                throw new ArgumentNullException();
            }
            this.dbFacadeFactory = dbFacadeFactory;
        }

        //retrieve all clients in the same district
        private List<Client> GetDistrictClients(SiteEngineer employee, IDatabaseFacade databaseFacade)
        {
            return databaseFacade.GetAll<Client>()
                .Where(c => c.District == employee.District).ToList();
        }

        //for site engineer index page
        //return a list of clients
        [HttpGet]
        public ActionResult Client()
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                var employee = (SiteEngineer)databaseFacade.GetEmployeeByUserId(User.Identity.GetUserId());
                if (employee != null)
                {
                    var viewModel = new SiteEngineerIndexViewModel()
                    {
                        DistrictClients = GetDistrictClients(employee, databaseFacade)
                    };

                    return View(viewModel);
                }
                else
                    return new HttpNotFoundResult();
            }


        }

        //return all interventions of a client with local variable
        private List<DataModel.Intervention> GetInterventionsByClientId(IDatabaseFacade databaseFacade, int id)
        {
            List<DataModel.Intervention> interventions = new List<DataModel.Intervention>();
            var temp = databaseFacade.GetAll<Intervention>().Where(i => (i.State != State.CANCEL) && (i.ClientId == id));
            foreach (var item in temp)
            {
                DataModel.Intervention intervention = new DataModel.Intervention()
                {
                    Id = item.Id,
                    Employee = item.Employee,
                    Client = item.Client,
                    Costs = item.Costs,
                    Duration = item.Duration,
                    InterventionType = item.InterventionType,
                    Created = item.Created
                };
                interventions.Add(intervention);
            }
            return interventions;
        }

        //for site engineer ClientDetail page
        //return a list of intervention of a client
        [HttpGet]
        [Route("Client/{id:int}/ClientDetail")]
        public ActionResult ClientDetail(int id)
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                client = GetClient(databaseFacade, id);
                if (client != null)
                {
                    var viewModel = new ClientDetailViewModel()
                    {
                        Client = client,
                        Interventions = GetInterventionsByClientId(databaseFacade, id)
                    };

                    return View(viewModel);
                }
                return new HttpNotFoundResult();
            }
        }

        //save a client to database
        private void AddClient(SiteEngineer employee, IDatabaseFacade databaseFacade, NewClientViewModel viewModel)
        {
            Client client = viewModel.Client;
            client.District = employee.District;

            try
            {
                databaseFacade.BeginTransaction();
                databaseFacade.SaveOrUpdate(client);
                databaseFacade.CommitTransaction();
            }
            catch (Exception)
            {
                databaseFacade.RollbackTransaction();
                throw;
            }
        }

        //site engineer NewClient view
        [HttpGet]
        public ActionResult NewClient()
        {
            return View();
        }

        //post a new client
        [HttpPost]
        public ActionResult NewClient(NewClientViewModel viewModel)
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                if (ModelState.IsValid)
                {
                    var employee = (SiteEngineer)databaseFacade.GetEmployeeByUserId(User.Identity.GetUserId());
                    try
                    {
                        AddClient(employee, databaseFacade, viewModel);
                        return RedirectToAction("Client", "Client");
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError(string.Empty, "Please complete the form.");
                    }
                }
                return View(viewModel);
            }

        }

        //get a client by its id
        private Client GetClient(IDatabaseFacade databaseFacade, int id)
        {
            return databaseFacade.GetById<Client>(id);
        }

        //return all interventiontypes from database
        private List<InterventionType> GetAllInterventionTypes()
        {
            using (var databaseFacade = dbFacadeFactory.Create())
                return databaseFacade.GetAll<InterventionType>();
        }

        //save an intervention to database
        private void AddIntervention(Employee employee, IDatabaseFacade databaseFacade, NewInterventionViewModel viewModel)
        {
            var intervention = new Intervention()
            {
                Costs = viewModel.Cost,
                Duration = viewModel.Duration,
                InterventionTypeId = viewModel.SelectedValue,
                Created = viewModel.CurrentDate,
                EmployeeId = employee.Id,
                Notes = "note",
                ClientId = client.Id
            };

            try
            {
                databaseFacade.BeginTransaction();
                databaseFacade.SaveOrUpdate(intervention);
                databaseFacade.CommitTransaction();
            }
            catch (Exception)
            {
                databaseFacade.RollbackTransaction();
                throw;
            }
        }

        //site engineer create new intervention page
        [HttpGet]
        [Route("Client/{id:int}/NewIntervention")]
        public ActionResult NewIntervention(int id, int interventionTypeId = -1)
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                Client client = GetClient(databaseFacade, id);
                var employee = (SiteEngineer)databaseFacade.GetEmployeeByUserId(User.Identity.GetUserId());
                if (client != null)
                {
                    var interventionTypes = GetAllInterventionTypes();

                    if (interventionTypeId < 0)
                    {
                        // use first intervention type as default selection
                        var firstIntervention = interventionTypes.FirstOrDefault();
                        if (firstIntervention != null)
                        {
                            interventionTypeId = firstIntervention.Id;
                        }
                    }

                    var selectedInterventionType = interventionTypes
                        .Where(interventionType => interventionType.Id == interventionTypeId)
                        .FirstOrDefault();

                    var viewModel = new NewInterventionViewModel()
                    {
                        InterventionType = new SelectList(GetAllInterventionTypes(), "Id", "Name"),
                        ClientName = client.Name,
                        SiteEngineer = employee.Name,
                        CurrentDate = DateTime.Now,
                        Duration = (selectedInterventionType != null) ? selectedInterventionType.Duration : default(decimal),
                        Cost = (selectedInterventionType != null) ? selectedInterventionType.Costs : default(decimal),
                        SelectedValue = (selectedInterventionType != null) ? interventionTypeId : default(int)
                    };

                    return View(viewModel);
                }
                return new HttpNotFoundResult();
            }
        }

        //site engineer post a new intervention
        [HttpPost]
        [Route("Client/{id:int}/NewIntervention")]
        public ActionResult NewIntervention(int id, bool refresh, NewInterventionViewModel viewModel)
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                if (refresh)
                {
                    return RedirectToAction("NewIntervention", new { id = id, interventionTypeId = viewModel.SelectedValue });
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        var employee = (SiteEngineer)databaseFacade.GetEmployeeByUserId(User.Identity.GetUserId());
                        client = GetClient(databaseFacade, id);
                        try
                        {
                            AddIntervention(employee, databaseFacade, viewModel);
                            return RedirectToAction("ClientDetail", "Client", id);
                        }
                        catch (Exception)
                        {
                            ModelState.AddModelError(string.Empty, "Please enter a valid number.");
                        }
                    }
                }
                viewModel.InterventionType = new SelectList(GetAllInterventionTypes(), "Id", "Name");
                return View(viewModel);
            }
        }
    }
}