﻿using ENETMVC.Database;
using ENETMVC.Database.Interfaces;
using ENETMVC.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ENETMVC.Models;

namespace ENETMVC.Controllers
{
    [Authorize(Roles = "Accountant")]
    public class EmployeeController : Controller
    {
        private IDatabaseFacadeFactory dbFacadeFactory;
        private ApprovableEmployee employee;


        public EmployeeController() : this(new DatabaseFacadeFactory())
        {
        }

        public EmployeeController(IDatabaseFacadeFactory dbFacadeFactory)
        {
            if (dbFacadeFactory == null)
            {
                throw new ArgumentNullException();
            }
            this.dbFacadeFactory = dbFacadeFactory;
        }

        public ActionResult Index()
        {
            using (var df = dbFacadeFactory.Create())
            {
                var viewModel = new AccountantViewModel()
                {
                    SiteEngineers = df.GetAll<SiteEngineer>(),
                    Managers = df.GetAll<Manager>()
                };
                return View(viewModel);
            }
        }

        private List<SelectListItem> getItems()
        {
            return new ReportController().getItems();

        }

        [Route("Employee/{id:int}/ChangeDistrict")]
        public ActionResult ChangeDistrict(int id)
        {
            ViewBag.District = getItems();
            setEmployee(dbFacadeFactory, id);
            if (getViewModel(employee) != null)
                return View(getViewModel(employee));
            return new HttpNotFoundResult();
        }

        [HttpPost]
        [Route("Employee/{id:int}/ChangeDistrict")]
        public ActionResult ChangeDistrict(int id, District District)
        {
            ViewBag.District = getItems();
            setEmployee(dbFacadeFactory, id);
            employee.District = District;
            update();
            return View(getViewModel(employee));

        }

        private void setEmployee(IDatabaseFacadeFactory dbFacadeFactory, int id)
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                employee = getEmployee(databaseFacade, id);
            }
        }

        private EmployeeViewModel getViewModel(ApprovableEmployee employee)
        {
            if (employee != null)
            {
                var viewModel = new EmployeeViewModel()
                {
                    Employee = employee
                };

                return viewModel;
            }
            return null;
        }

        private void update()
        {
            using (var databaseFacade = dbFacadeFactory.Create())
            {
                try
                {
                    databaseFacade.BeginTransaction();
                    if (employee is Manager)
                    {
                        var manager = (Manager)employee;
                        databaseFacade.SaveOrUpdate(manager);
                    }
                    else
                    {
                        var siteEngineer = (SiteEngineer)employee;
                        databaseFacade.SaveOrUpdate(siteEngineer);
                    }
                    databaseFacade.CommitTransaction();
                }
                catch (Exception)
                {
                    databaseFacade.RollbackTransaction();
                }
            }
        }

        private SiteEngineer getSiteEngineer(IDatabaseFacade databaseFacade, int id)
        {
            return databaseFacade.GetById<SiteEngineer>(id);
        }

        private Manager getManager(IDatabaseFacade databaseFacade, int id)
        {
            return databaseFacade.GetById<Manager>(id);
        }

        private ApprovableEmployee getEmployee(IDatabaseFacade databaseFacade, int id)
        {
            if (getSiteEngineer(databaseFacade, id) != null)
                return getSiteEngineer(databaseFacade, id);
            return getManager(databaseFacade, id);
        }


    }


}

