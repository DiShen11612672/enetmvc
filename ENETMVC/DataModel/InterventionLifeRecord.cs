﻿using ENETMVC.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENETMVC.DataModel
{
    public class InterventionLifeRecord
    {
        public decimal RemainingLife { get; set; }

        public DateTime DateOfVisit { get; set; }
        public string Notes { get; set; }

        public string Employee { get; set; }

        public InterventionLifeRecord()
        {
        }

    }
}