﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENETMVC.DataModel
{
    public class InterventionState
    {
        public string Employee { get; set; }
        public string State { get; set; }
        public DateTime Created { get; set; }
    }
}