﻿using ENETMVC.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ENETMVC.DataModel
{
    public class Intervention
    {
        public int Id { get; set; }
        public decimal Duration { get; set; }

        public decimal Costs { get; set; }
        public Client Client { get; set; }

        public SiteEngineer Employee { get; set; }

        public InterventionType InterventionType { get; set; }
        public DateTime Created { get; set; }

        public List<InterventionState> StateRecords { get; set; }
        public List<InterventionLifeRecord> LifeRecords { get; set; }
    }
}