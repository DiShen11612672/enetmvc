﻿using System;
using System.Linq;
using System.Collections.Generic;
using ENETMVC.Database.Models;
using ENETMVC.Database.Repositories;
using ENETMVC.Database.Interfaces;
using ENETCMVC.Database.Repositories.Interfaces;

namespace ENETMVC.Database
{
    public class DatabaseFacade : IDatabaseFacade
    {
        private static Dictionary<Type, object> repositoryDictionary;

        static DatabaseFacade()
        {
            repositoryDictionary = new Dictionary<Type, object>();
            repositoryDictionary.Add(typeof(Accountant), new AccountantRepository());
            repositoryDictionary.Add(typeof(Client), new ClientRepository());
            repositoryDictionary.Add(typeof(Employee), new EmployeeRepository());
            repositoryDictionary.Add(typeof(InterventionLifeRecord), new InterventionLifeRecordRepository());
            repositoryDictionary.Add(typeof(Intervention), new InterventionRepository());
            repositoryDictionary.Add(typeof(InterventionStateRecord), new InterventionStateRecordRepository());
            repositoryDictionary.Add(typeof(InterventionType), new InterventionTypeRepository());
            repositoryDictionary.Add(typeof(Manager), new ManagerRepository());
            repositoryDictionary.Add(typeof(SiteEngineer), new SiteEngineerRepository());
        }

        private IMSDbContext dbContext;

        public DatabaseFacade()
        {
            dbContext = new IMSDbContext();
        }

        private IRepository<T> GetRepository<T>() where T : BaseEntity, new()
        {
            object obj;
            repositoryDictionary.TryGetValue(typeof(T), out obj);
            return (IRepository<T>)obj;
        }

        /// <summary>
        /// begins a new transaction
        /// </summary>
        public void BeginTransaction()
        {
        }

        /// <summary>
        /// rollbacks the current transaction
        /// </summary>
        public void RollbackTransaction()
        {
        }

        /// <summary>
        /// commits the current transaction
        /// </summary>
        public void CommitTransaction()
        {
            dbContext.SaveChanges();
        }

        /// <summary>
        /// retrieves all objects with the given type from the database
        /// </summary>
        /// <typeparam name="T">type of the objects to retrieve</typeparam>
        /// <returns>a list of objects of type T</returns>
        public List<T> GetAll<T>() where T : BaseEntity, new()
        {
            var repository = GetRepository<T>();
            if (repository != null)
            {
                return repository.GetAll(dbContext).ToList();
            }
            return new List<T>();
        }

        /// <summary>
        /// retrieves an object with the given type and id from the database
        /// </summary>
        /// <typeparam name="T">type of the object to retrieve</typeparam>
        /// <param name="id">id of the object</param>
        /// <returns>an object of type T</returns>
        public T GetById<T>(int id) where T : BaseEntity, new()
        {
            var repository = GetRepository<T>();
            if (repository != null)
            {
                return repository.GetById(dbContext, id).FirstOrDefault();
            }
            return null;
        }

        /// <summary>
        /// saves or updates an object in the database
        /// </summary>
        /// <typeparam name="T">type of the object to save or update</typeparam>
        /// <param name="entity">object to save or update</param>
        public void SaveOrUpdate<T>(T entity) where T : BaseEntity, new()
        {
            var repository = GetRepository<T>();
            if (repository != null)
            {
                repository.SaveOrUpdate(dbContext, entity);
            }
        }

        /// <summary>
        /// closes the current database connection
        /// </summary>
        public void Dispose()
        {
            if (dbContext != null)
            {
                dbContext.Dispose();
            }
        }

        /// <summary>
        /// retrieves an employee by its associated user id
        /// </summary>
        /// <param name="userId">user id of the employee to retrieve</param>
        /// <returns>employee with the associated user id</returns>
        public Employee GetEmployeeByUserId(string userId)
        {
            return ((EmployeeRepository)GetRepository<Employee>()).GetByUserId(dbContext, userId);
        }

        /// <summary>
        /// retrieves all intervention state records by their associated intervention id
        /// </summary>
        /// <param name="interventionId">id of the intervention</param>
        /// <returns>a list of intervention state records</returns>
        public List<InterventionStateRecord> GetInterventionStateRecordsByInterventionId(int interventionId)
        {
            return ((InterventionStateRecordRepository)GetRepository<InterventionStateRecord>()).GetByInterventionId(dbContext, interventionId);
        }

        /// <summary>
        /// retrieves all intervention life records by their associated intervention id
        /// </summary>
        /// <param name="interventionId">id of the intervention</param>
        /// <returns>a list of intervention life records</returns>
        public List<InterventionLifeRecord> GetInterventionLifeRecordsByInterventionId(int interventionId)
        {
            return ((InterventionLifeRecordRepository)GetRepository<InterventionLifeRecord>()).GetByInterventionId(dbContext, interventionId);
        }

        public List<Intervention> GetInterventionsByEmployeeId(int employeeId)
        {
            return ((InterventionRepository)GetRepository<Intervention>()).GetByEmployeeId(dbContext, employeeId);
        }
    }
}
