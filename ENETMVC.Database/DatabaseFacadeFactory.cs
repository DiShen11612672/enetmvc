﻿using ENETMVC.Database.Interfaces;

namespace ENETMVC.Database
{
    public class DatabaseFacadeFactory : IDatabaseFacadeFactory
    {
        public IDatabaseFacade Create()
        {
            return new DatabaseFacade();
        }
    }
}
