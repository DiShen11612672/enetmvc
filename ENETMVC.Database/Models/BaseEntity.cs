﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ENETMVC.Database.Models
{
    public abstract class BaseEntity
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            var baseEntity = (BaseEntity)obj;
            return Id.Equals(baseEntity.Id);
        }

        public override int GetHashCode()
        {
            return GetType().GetHashCode() ^ Id.GetHashCode();
        }
    }
}