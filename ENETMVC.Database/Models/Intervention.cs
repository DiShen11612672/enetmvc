namespace ENETMVC.Database.Models
{
    using Exceptions;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Intervention")]
    public partial class Intervention : BaseEntity
    {
        private InterventionState state;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Intervention()
        {
            InterventionLifeRecords = new HashSet<InterventionLifeRecord>();
            InterventionStateRecords = new HashSet<InterventionStateRecord>();
            state = new InterventionStateProposed(this);
            Created = DateTime.Now;
        }

        public Intervention(SiteEngineer employee, Client client, InterventionType interventionType) : this(employee, client, interventionType, State.PROPOSE)
        {
        }

        public Intervention(SiteEngineer employee, Client client, InterventionType interventionType, State state) : this()
        {
            if ((employee == null) || (client == null) || (interventionType == null))
            {
                throw new ArgumentNullException();
            }

            if (!employee.District.Equals(client.District))
            {
                throw new DistrictMismatchException();
            }

            Employee = employee;
            Client = client;
            InterventionType = interventionType;
            Created = DateTime.Now;

            switch (state)
            {
                case State.PROPOSE:
                    this.state = new InterventionStateProposed(this);
                    Duration = interventionType.Duration;
                    Costs = interventionType.Costs;
                    break;
                case State.APPROVE:
                    this.state = new InterventionStateApproved(this);
                    break;
                case State.CANCEL:
                    this.state = new InterventionStateCancelled(this);
                    break;
                case State.COMPLETE:
                    this.state = new InterventionStateCompleted(this);
                    break;
            }
        }

        public int EmployeeId { get; set; }

        public int ClientId { get; set; }

        public int InterventionTypeId { get; set; }

        public decimal Duration { get; set; }

        public decimal Costs { get; set; }

        [Required]
        [StringLength(512)]
        public string Notes { get; set; }

        public DateTime Created { get; set; }

        [Required]
        [StringLength(256)]
        [Column("State")]
        public string StateString
        {
            get { return State.ToString(); }
            private set { State = (State)Enum.Parse(typeof(State), value, true); }
        }

        [NotMapped]
        public State State
        {
            get { return state.State; }
            private set
            {
                switch (value)
                {
                    case State.PROPOSE:
                        state = new InterventionStateProposed(this);
                        break;
                    case State.APPROVE:
                        state = new InterventionStateApproved(this);
                        break;
                    case State.CANCEL:
                        state = new InterventionStateCancelled(this);
                        break;
                    case State.COMPLETE:
                        state = new InterventionStateCompleted(this);
                        break;
                }
            }
        }

        public virtual Client Client { get; set; }

        public virtual SiteEngineer Employee { get; set; }

        public virtual InterventionType InterventionType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InterventionLifeRecord> InterventionLifeRecords { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InterventionStateRecord> InterventionStateRecords { get; set; }

        public void Approve(ApprovableEmployee employee)
        {
            state.Approve(employee);
            state = new InterventionStateApproved(this);
        }

        public void Cancel(ApprovableEmployee employee)
        {
            state.Cancel(employee);
            state = new InterventionStateCancelled(this);
        }

        public void Complete(ApprovableEmployee employee)
        {
            state.Complete(employee);
            state = new InterventionStateCompleted(this);
        }

        public List<State> GetPossibleInterventionStates()
        {
            var possibleInterventionStates = new List<State>();
            if (state.Approvable)
            {
                possibleInterventionStates.Add(State.APPROVE);
            }
            if (state.Completable)
            {
                possibleInterventionStates.Add(State.COMPLETE);
            }
            if (state.Cancellable)
            {
                possibleInterventionStates.Add(State.CANCEL);
            }
            return possibleInterventionStates;
        }
    }
}
