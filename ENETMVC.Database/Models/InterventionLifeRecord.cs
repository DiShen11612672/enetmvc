namespace ENETMVC.Database.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("InterventionLifeRecord")]
    public partial class InterventionLifeRecord : BaseEntity
    {
        public int InterventionId { get; set; }

        public int EmployeeId { get; set; }

        public decimal RemainingLife { get; set; }

        public DateTime DateOfVisit { get; set; }

        [Required]
        [StringLength(512)]
        public string Notes { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Intervention Intervention { get; set; }

        public InterventionLifeRecord()
        {
        }

        public InterventionLifeRecord(Intervention intervention, Employee employee, decimal remainingLife, string notes)
        {
            if ((intervention == null) || (employee == null))
            {
                throw new ArgumentNullException();
            }

            InterventionId = intervention.Id;
            Employee = employee;
            RemainingLife = remainingLife;
            DateOfVisit = DateTime.Now;
            Notes = notes;
        }
    }
}
