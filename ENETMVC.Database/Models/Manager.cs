namespace ENETMVC.Database.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Manager")]
    public partial class Manager : ApprovableEmployee
    {
        // List of proposed interventions to be approved.
        [NotMapped]
        public List<Intervention> ProposedInterventions
        {
            get;
            private set;
        }

        // List of previously approved interventions.
        [NotMapped]
        public List<Intervention> PastInterventions
        {
            get;
            private set;
        }
        /// <summary>
        /// Default constructor.
        /// Creates lists to store proposed interventions and
        /// interventions approved by a particular manager.
        /// </summary>
        public Manager()
        {
            ProposedInterventions = new List<Intervention>();
            PastInterventions = new List<Intervention>();
        }

        /// <summary>
        /// Constuctor to create a manager object explicitly defining all data members.
        /// Used for testing.
        /// </summary>
        /// <param name="id">Database primary key.</param>
        /// <param name="name">Real-world name of manager.</param>
        /// <param name="uname">Username for manager.</param>
        /// <param name="dist">District the manager is assigned to.</param>
        /// <param name="maxDur">Maximum duration that a manager can approve on an intervention.</param>
        /// <param name="maxCosts">Maximum cost of materials a manager can approve on an intervention.</param>
        public Manager(int id, string name, string uname, District dist, decimal maxDur, decimal maxCosts) : this()
        {
            Id = id;
            Name = name;
            UserId = uname;
            District = dist;
            MaxDuration = maxDur;
            MaxCosts = maxCosts;
        }
    }
}