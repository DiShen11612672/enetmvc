namespace ENETMVC.Database.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Client")]
    public partial class Client : BaseEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Client()
        {
            Interventions = new HashSet<Intervention>();
        }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        [Required]
        [StringLength(256)]
        public string Location { get; set; }

        [Required]
        [StringLength(256)]
        [Column("District")]
        public string DistrictString
        {
            get { return District.ToString(); }
            private set { District = (District)Enum.Parse(typeof(District), value, true); }
        }

        [NotMapped]
        public District District { get; set; }

        [Required]
        [StringLength(256)]
        [Column("ClientType")]
        public string ClientTypeString
        {
            get { return ClientType.ToString(); }
            private set { ClientType = (ClientType)Enum.Parse(typeof(ClientType), value, true); }
        }

        [NotMapped]
        public ClientType ClientType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Intervention> Interventions { get; set; }
    }
}
