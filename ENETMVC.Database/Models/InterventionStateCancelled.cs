﻿using System;

namespace ENETMVC.Database.Models
{
    class InterventionStateCancelled : InterventionState
    {
        public override State State { get { return State.CANCEL; } }

        public InterventionStateCancelled(Intervention intervention) : base(intervention)
        {
        }

        public override void Approve(ApprovableEmployee employee)
        {
            base.Approve(employee);
            throw new NotImplementedException("A cancelled intervention cannot be approved!");
        }

        public override void Cancel(ApprovableEmployee employee)
        {
            base.Cancel(employee);
            throw new NotImplementedException("A cancelled intervention cannot be cancelled again!");
        }

        public override void Complete(ApprovableEmployee employee)
        {
            base.Complete(employee);
            throw new NotImplementedException("A cancelled intervention cannot be completed!");
        }
    }
}