﻿using ENETMVC.Database.Models.Exceptions;
using System;

namespace ENETMVC.Database.Models
{
    public abstract class InterventionState
    {
        public abstract State State { get; }

        public Intervention Intervention { get; }

        public virtual bool Approvable { get { return false; } }

        public virtual bool Cancellable { get { return false; } }

        public virtual bool Completable { get { return false; } }

        public InterventionState(Intervention intervention)
        {
            if (intervention == null)
            {
                throw new ArgumentNullException();
            }
            Intervention = intervention;
        }

        public virtual void Approve(ApprovableEmployee employee)
        {
            if (!this.Approvable || (employee == null))
            {
                throw new InterventionStateChangeException();
            }
        }

        public virtual void Cancel(ApprovableEmployee employee)
        {
            if (!this.Cancellable || (employee == null))
            {
                throw new InterventionStateChangeException();
            }
        }

        public virtual void Complete(ApprovableEmployee employee)
        {
            if (!this.Completable || (employee == null))
            {
                throw new InterventionStateChangeException();
            }
        }
    }
}