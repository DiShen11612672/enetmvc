﻿namespace ENETMVC.Database.Models
{
    public enum State
    {
        PROPOSE,
        APPROVE,
        CANCEL,
        COMPLETE
    }
}
