﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ENETMVC.Database.Models
{
    public abstract class ApprovableEmployee : Employee
    {
        [Required]
        [StringLength(256)]
        [Column("District")]
        public string DistrictString
        {
            get { return District.ToString(); }
            private set { District = (District)Enum.Parse(typeof(District), value, true); }
        }

        [NotMapped]
        public District District { get; set; }

        public decimal MaxDuration { get; set; }

        public decimal MaxCosts { get; set; }
    }
}
