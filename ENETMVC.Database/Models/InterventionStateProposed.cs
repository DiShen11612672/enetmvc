﻿using ENETMVC.Database.Models.Exceptions;
using System;

namespace ENETMVC.Database.Models
{
    class InterventionStateProposed : InterventionState
    {
        public override State State { get { return State.PROPOSE; } }

        public override bool Approvable { get { return true; } }

        public override bool Cancellable { get { return true; } }

        public InterventionStateProposed(Intervention intervention) : base(intervention)
        {
        }

        public override void Approve(ApprovableEmployee employee)
        {
            base.Approve(employee);

            if (employee is Manager)
            {
                Approve((Manager)employee);
            }
            else if (employee is SiteEngineer)
            {
                Approve((SiteEngineer)employee);
            }
        }

        public override void Cancel(ApprovableEmployee employee)
        {
            base.Cancel(employee);

            if (employee is Manager)
            {
                Cancel((Manager)employee);
            }
            else if (employee is SiteEngineer)
            {
                Cancel((SiteEngineer)employee);
            }
        }

        public override void Complete(ApprovableEmployee employee)
        {
            base.Complete(employee);
            throw new NotImplementedException("A proposed intervention cannot be completed!");
        }

        private void Approve(Manager manager)
        {
            // check if manager works in the same district as the client
            if (!manager.District.Equals(Intervention.Client.District))
            {
                throw new InterventionStateChangeException();
            }

            CheckApprovability(manager);
        }

        private void Approve(SiteEngineer siteEngineer)
        {
            // check if intervention is approved by the site engineer that proposed the intervention
            if (!siteEngineer.Id.Equals(Intervention.Employee.Id))
            {
                throw new InterventionStateChangeException();
            }

            CheckApprovability(siteEngineer);
        }

        private void Cancel(Manager manager)
        {
            // check if manager works in the same district as the site engineer that proposed the intervention
            if (!manager.District.Equals(Intervention.Client.District))
            {
                throw new InterventionStateChangeException();
            }

            CheckApprovability(manager);
        }

        private void Cancel(SiteEngineer siteEngineer)
        {
            // check if intervention is cancelled by the site engineer that proposed the intervention
            if (!siteEngineer.Id.Equals(Intervention.Employee.Id))
            {
                throw new InterventionStateChangeException();
            }

            CheckApprovability(siteEngineer);
        }

        private void CheckApprovability(ApprovableEmployee employee)
        {
            if ((Intervention.Costs <= employee.MaxCosts) && (Intervention.Duration <= employee.MaxDuration))
            {
                var interventionType = Intervention.InterventionType;
                if ((interventionType.Costs <= employee.MaxCosts) && (interventionType.Duration <= employee.MaxDuration))
                {
                    return;
                }
            }
            throw new InterventionStateChangeException();
        }
    }
}