namespace ENETMVC.Database.Models
{
    using Interfaces;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("InterventionStateRecord")]
    public partial class InterventionStateRecord : BaseEntity
    {
        public int InterventionId { get; set; }

        public int EmployeeId { get; set; }

        [Required]
        [StringLength(256)]
        [Column("State")]
        public string StateString
        {
            get { return State.ToString(); }
            private set { State = (State)Enum.Parse(typeof(State), value, true); }
        }

        [NotMapped]
        public State State { get; set; }

        public DateTime Created { get; set; }

        public virtual ApprovableEmployee Employee { get; set; }

        public virtual Intervention Intervention { get; set; }
    }
}
