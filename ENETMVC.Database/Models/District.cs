﻿namespace ENETMVC.Database.Models
{
    public enum District
    {
        RURAL_INDONESIA,
        RURAL_NSW,
        RURAL_PNG,
        SYDNEY,
        URBAN_INDONESIA,
        URBAN_PNG
    }
}
