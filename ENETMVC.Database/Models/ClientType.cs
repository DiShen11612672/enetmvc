﻿namespace ENETMVC.Database.Models
{
    public enum ClientType
    {
        PERSON,
        FAMILY,
        COMMUNITY
    }
}
