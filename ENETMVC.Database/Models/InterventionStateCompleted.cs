﻿using System;

namespace ENETMVC.Database.Models
{
    class InterventionStateCompleted : InterventionState
    {
        public override State State { get { return State.COMPLETE; } }

        public InterventionStateCompleted(Intervention intervention) : base(intervention)
        {
        }

        public override void Approve(ApprovableEmployee employee)
        {
            base.Approve(employee);
            throw new NotImplementedException("A completed intervention cannot be approved!");
        }

        public override void Cancel(ApprovableEmployee employee)
        {
            base.Cancel(employee);
            throw new NotImplementedException("A completed intervention cannot be cancelled!");
        }

        public override void Complete(ApprovableEmployee employee)
        {
            base.Complete(employee);
            throw new NotImplementedException("A completed intervention cannot be completed again!");
        }
    }
}