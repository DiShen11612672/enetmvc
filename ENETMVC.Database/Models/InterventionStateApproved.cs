﻿using ENETMVC.Database.Models.Exceptions;
using System;

namespace ENETMVC.Database.Models
{
    class InterventionStateApproved : InterventionState
    {
        public override State State { get { return State.APPROVE; } }

        public override bool Cancellable { get { return true; } }

        public override bool Completable { get { return true; } }

        public InterventionStateApproved(Intervention intervention) : base(intervention)
        {
        }

        public override void Approve(ApprovableEmployee employee)
        {
            base.Approve(employee);
            throw new NotImplementedException("An approved intervention cannot be approved again!");
        }

        public override void Cancel(ApprovableEmployee employee)
        {
            base.Cancel(employee);
            CheckIdentity(employee);
        }

        public override void Complete(ApprovableEmployee employee)
        {
            base.Complete(employee);
            CheckIdentity(employee);
        }

        private void CheckIdentity(ApprovableEmployee employee)
        {
            if (employee != null)
            {
                // check if employee is the site engineer that proposed the intervention
                if ((employee is SiteEngineer) && (Intervention.Employee.Id.Equals(employee.Id)))
                {
                    return;
                }
            }
            throw new InterventionStateChangeException();
        }
    }
}