﻿namespace ENETMVC.Database.Interfaces
{
    public interface IDatabaseFacadeFactory
    {
        IDatabaseFacade Create();
    }
}
