﻿using ENETMVC.Database.Models;
using System;
using System.Collections.Generic;

namespace ENETMVC.Database.Interfaces
{
    public interface IDatabaseFacade : IDisposable
    {
        void BeginTransaction();

        void RollbackTransaction();

        void CommitTransaction();

        List<T> GetAll<T>() where T : BaseEntity, new();

        T GetById<T>(int id) where T : BaseEntity, new();

        void SaveOrUpdate<T>(T obj) where T : BaseEntity, new();

        Employee GetEmployeeByUserId(string userId);

        List<InterventionStateRecord> GetInterventionStateRecordsByInterventionId(int interventionId);

        List<InterventionLifeRecord> GetInterventionLifeRecordsByInterventionId(int interventionId);

        List<Intervention> GetInterventionsByEmployeeId(int employeeId);
    }
}
