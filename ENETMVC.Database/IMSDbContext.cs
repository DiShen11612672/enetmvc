namespace ENETMVC.Database
{
    using Models;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    public partial class IMSDbContext : DbContext
    {
        public IMSDbContext()
            : base("name=IMSDbContext")
        {
        }

        public virtual DbSet<Accountant> Accountants { get; set; }
        public virtual DbSet<ApprovableEmployee> ApprovableEmployees { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Intervention> Interventions { get; set; }
        public virtual DbSet<InterventionLifeRecord> InterventionLifeRecords { get; set; }
        public virtual DbSet<InterventionStateRecord> InterventionStateRecords { get; set; }
        public virtual DbSet<InterventionType> InterventionTypes { get; set; }
        public virtual DbSet<Manager> Managers { get; set; }
        public virtual DbSet<SiteEngineer> SiteEngineers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>()
                .HasMany(e => e.Interventions)
                .WithRequired(e => e.Client)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.InterventionLifeRecords)
                .WithRequired(e => e.Employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.InterventionStateRecords)
                .WithRequired(e => e.Employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Intervention>()
                .Property(e => e.Duration)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Intervention>()
                .Property(e => e.Costs)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Intervention>()
                .HasRequired(e => e.Employee)
                .WithMany(e => e.Interventions)
                .HasForeignKey(e => e.EmployeeId);

            modelBuilder.Entity<Intervention>()
                .HasMany(e => e.InterventionLifeRecords)
                .WithRequired(e => e.Intervention)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Intervention>()
                .HasMany(e => e.InterventionStateRecords)
                .WithRequired(e => e.Intervention)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InterventionLifeRecord>()
                .Property(e => e.RemainingLife)
                .HasPrecision(5, 2);

            modelBuilder.Entity<InterventionType>()
                .Property(e => e.Duration)
                .HasPrecision(10, 2);

            modelBuilder.Entity<InterventionType>()
                .Property(e => e.Costs)
                .HasPrecision(10, 2);

            modelBuilder.Entity<InterventionType>()
                .HasMany(e => e.Interventions)
                .WithRequired(e => e.InterventionType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Manager>()
                .Map(m =>
                {
                    m.Properties(t => new { t.DistrictString, t.MaxCosts, t.MaxDuration });
                    m.ToTable("Manager");
                });

            modelBuilder.Entity<Manager>()
                .Property(e => e.MaxDuration)
                .HasPrecision(10, 2);

            modelBuilder.Entity<Manager>()
                .Property(e => e.MaxCosts)
                .HasPrecision(10, 2);

            modelBuilder.Entity<SiteEngineer>()
                .Map(m =>
                {
                    m.Properties(t => new { t.DistrictString, t.MaxCosts, t.MaxDuration });
                    m.ToTable("SiteEngineer");
                });

            modelBuilder.Entity<SiteEngineer>()
                .HasMany(e => e.Interventions)
                .WithRequired(e => e.Employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SiteEngineer>()
                .Property(e => e.MaxDuration)
                .HasPrecision(10, 2);

            modelBuilder.Entity<SiteEngineer>()
                .Property(e => e.MaxCosts)
                .HasPrecision(10, 2);
        }
    }
}
