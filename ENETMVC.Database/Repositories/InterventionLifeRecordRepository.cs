﻿using ENETMVC.Database.Models;
using System.Collections.Generic;
using System.Linq;

namespace ENETMVC.Database.Repositories
{
    public class InterventionLifeRecordRepository : AbstractRepository<InterventionLifeRecord>
    {
        public List<InterventionLifeRecord> GetByInterventionId(IMSDbContext dbContext, int interventionId)
        {
            return GetAll(dbContext)
                .Where(interventionLifeRecord => interventionLifeRecord.InterventionId == interventionId)
                .ToList();
        }
    }
}
