﻿using ENETMVC.Database.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace ENETMVC.Database.Repositories
{
    public class InterventionRepository : AbstractRepository<Intervention>
    {
        public List<Intervention> GetByEmployeeId(DbContext dbContext, int employeeId)
        {
            return GetAll(dbContext)
                .Where(intervention => intervention.EmployeeId == employeeId)
                .ToList();
        }
    }
}
