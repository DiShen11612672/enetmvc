﻿using ENETMVC.Database.Models;
using System.Data.Entity;
using System.Linq;

namespace ENETCMVC.Database.Repositories.Interfaces
{
    public interface IRepository<Entity> where Entity : BaseEntity, new()
    {
        IQueryable<Entity> GetAll(DbContext dbContext);

        IQueryable<Entity> GetById(DbContext dbContext, int id);

        void SaveOrUpdate(DbContext dbContext, Entity entity);
    }
}
