﻿using ENETMVC.Database.Models;
using System;
using System.Data.Entity;
using System.Linq;

namespace ENETMVC.Database.Repositories
{
    public class EmployeeRepository : AbstractRepository<Employee>
    {
        public Employee GetByUserId(DbContext dbContext, string userId)
        {
            return GetAll(dbContext)
                .Where(employee => employee.UserId == userId)
                .FirstOrDefault();
        }
    }
}
