﻿using ENETMVC.Database.Models;
using System.Collections.Generic;
using System.Linq;

namespace ENETMVC.Database.Repositories
{
    public class InterventionStateRecordRepository : AbstractRepository<InterventionStateRecord>
    {
        public List<InterventionStateRecord> GetByInterventionId(IMSDbContext dbContext, int interventionId)
        {
            return GetAll(dbContext)
                .Where(interventionStateRecord => interventionStateRecord.InterventionId == interventionId)
                .ToList();
        }
    }
}
