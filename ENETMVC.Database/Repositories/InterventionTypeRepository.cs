﻿using ENETMVC.Database.Models;

namespace ENETMVC.Database.Repositories
{
    public class InterventionTypeRepository : AbstractRepository<InterventionType>
    {
    }
}
