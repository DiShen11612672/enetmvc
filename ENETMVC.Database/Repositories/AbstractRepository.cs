﻿using ENETCMVC.Database.Repositories.Interfaces;
using ENETMVC.Database.Models;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace ENETMVC.Database.Repositories
{
    public abstract class AbstractRepository<Entity> : IRepository<Entity> where Entity : BaseEntity, new()
    {
        public virtual IQueryable<Entity> GetAll(DbContext dbContext)
        {
            return dbContext.Set<Entity>();
        }

        public virtual IQueryable<Entity> GetById(DbContext dbContext, int id)
        {
            return dbContext.Set<Entity>().Where(entity => entity.Id == id);
        }

        public virtual void SaveOrUpdate(DbContext dbContext, Entity entity)
        {
            if (entity != null)
            {
                if (entity.Id > 0)
                {
                    var oldEntity = GetById(dbContext, entity.Id).FirstOrDefault();
                    if (oldEntity != null)
                    {
                        Update(dbContext, oldEntity, entity);
                        return;
                    }
                }
                Insert(dbContext, entity);
            }
        }

        private void Update(DbContext dbContext, Entity oldEntity, Entity entity)
        {
            dbContext.Entry(oldEntity).CurrentValues.SetValues(entity);
        }

        private void Insert(DbContext dbContext, Entity entity)
        {
            dbContext.Set<Entity>().Add(entity);
        }
    }
}
