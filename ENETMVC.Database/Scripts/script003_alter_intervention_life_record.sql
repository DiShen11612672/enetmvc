﻿ALTER TABLE [InterventionLifeRecord]
	ADD [Notes] NVARCHAR (512) NOT NULL DEFAULT '';
GO

UPDATE [InterventionLifeRecord]
	SET Notes = '';
GO