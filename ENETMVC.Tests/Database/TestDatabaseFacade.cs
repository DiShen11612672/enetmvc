﻿using ENETMVC.Database;
using ENETMVC.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace ENETMVC.IMSTests.Database
{
    [TestClass]
    public class TestDatabaseFacade
    {
        [ClassInitialize]
        public static void SetupSuite(TestContext testContext)
        {
            // set path to database file
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\ENETMVC.Database\Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        //[TestMethod]
        [TestCategory("DatabaseFacade")]
        public void GetAll_WithTransaction_ShouldReturnAllClients()
        {
            // act
            using (var databaseFacade = new DatabaseFacade())
            {
                databaseFacade.BeginTransaction();
                var clientList = databaseFacade.GetAll<Client>();
                databaseFacade.CommitTransaction();

                // assert
                Assert.AreEqual(8, clientList.Count);
            }
        }

        //[TestMethod]
        [TestCategory("DatabaseFacade")]
        public void GetAll_WithoutTransaction_ShouldReturnAllClients()
        {
            // act
            using (var databaseFacade = new DatabaseFacade())
            {
                var clientList = databaseFacade.GetAll<Client>();

                // assert
                Assert.AreEqual(8, clientList.Count);
            }
        }

        //[TestMethod]
        [TestCategory("DatabaseFacade")]
        public void GetEmployeeByUserId_WithoutTransaction_ShouldReturnEmployee()
        {
            // actd
            using (var databaseFacade = new DatabaseFacade())
            {
                var employee = databaseFacade.GetEmployeeByUserId("0c5bfb3d-f37d-4e8b-8407-85c27b256b33");

                // assert
                Assert.AreEqual(1, employee.Id);
                Assert.AreEqual("0c5bfb3d-f37d-4e8b-8407-85c27b256b33", employee.UserId);
                Assert.AreEqual("Ben Stevens", employee.Name);
            }
        }
    }
}
