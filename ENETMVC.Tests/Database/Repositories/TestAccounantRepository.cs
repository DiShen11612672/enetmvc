﻿using ENETMVC.Database;
using ENETMVC.Database.Models;
using ENETMVC.Database.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace ENETMVC.IMSTests.Database.Repositories
{
    [TestClass]
    public class TestAccounantRepository
    {
        private AccountantRepository accountantRepository;

        [ClassInitialize]
        public static void SetupSuite(TestContext testContext)
        {
            // set path to database file
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\ENETMVC.Database\Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        [TestInitialize]
        public void Setup()
        {
            accountantRepository = new AccountantRepository();
        }

        //[TestMethod]
        [TestCategory("AccountantRepository")]
        public void GetAll_ShouldReturnAllAccountants()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var accountants = accountantRepository.GetAll(dbContext).ToList();

                // assert
                Assert.AreEqual(2, accountants.Count);
                Assert.AreEqual(2, accountants[0].Id);
                Assert.AreEqual("Carol", accountants[0].Name);
                Assert.AreEqual("7306ce93-909b-4998-941c-3fd3844fe667", accountants[0].UserId);
                Assert.AreEqual(18, accountants[1].Id);
                Assert.AreEqual("Patrick", accountants[1].Name);
                Assert.AreEqual("fbf49f00-582a-430f-8d89-b877bec8cd88", accountants[1].UserId);
            }
        }

        //[TestMethod]
        [TestCategory("AccountantRepository")]
        public void GetById_ShouldReturnAccountant()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var accountant = accountantRepository.GetById(dbContext, 18).FirstOrDefault();

                // assert
                Assert.AreEqual(18, accountant.Id);
                Assert.AreEqual("Patrick", accountant.Name);
                Assert.AreEqual("fbf49f00-582a-430f-8d89-b877bec8cd88", accountant.UserId);
            }
        }

        //[TestMethod]
        [TestCategory("AccountantRepository")]
        public void SaveOrUpdate_ShouldUpdateExistingAccountant()
        {
            // arrange
            var accountant = new Accountant()
            {
                Id = 18,
                Name = "Patrick",
                UserId = "fbf49f00-582a-430f-8d89-b877bec8cd88"
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                accountantRepository.SaveOrUpdate(dbContext, accountant);
                dbContext.SaveChanges();
            }
        }

        //[TestMethod]
        [TestCategory("AccountantRepository")]
        public void SaveOrUpdate_ShouldInsertNewAccountant()
        {
            // arrange
            var accountant = new Accountant()
            {
                Name = "Patrick Schedler",
                UserId = "fbf49f00-582a-430f-8d89-b877bec8cd88"
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                accountantRepository.SaveOrUpdate(dbContext, accountant);
                dbContext.SaveChanges();
            }
        }
    }
}