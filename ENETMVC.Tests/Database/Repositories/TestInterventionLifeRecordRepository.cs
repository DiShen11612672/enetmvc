﻿using ENETMVC.Database;
using ENETMVC.Database.Models;
using ENETMVC.Database.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace ENETMVC.IMSTests.Database.Repositories
{
    [TestClass]
    public class TestInterventionLifeRecordRepository
    {
        private InterventionLifeRecordRepository interventionLifeRecordRepository;

        [ClassInitialize]
        public static void SetupSuite(TestContext testContext)
        {
            // set path to database file
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\ENETMVC.Database\Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        [TestInitialize]
        public void Setup()
        {
            interventionLifeRecordRepository = new InterventionLifeRecordRepository();
        }

        //[TestMethod]
        [TestCategory("InterventionLifeRecordRepository")]
        public void GetAll_ShouldReturnAllInterventionLifeRecords()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var interventionLifeRecords = interventionLifeRecordRepository.GetAll(dbContext).ToList();

                // assert
                Assert.AreEqual(2, interventionLifeRecords.Count);
                Assert.AreEqual(1, interventionLifeRecords[0].Id);

                Assert.AreEqual(1, interventionLifeRecords[0].InterventionId);
                Assert.IsNotNull(interventionLifeRecords[0].Intervention);

                Assert.AreEqual(11, interventionLifeRecords[0].EmployeeId);
                Assert.IsNotNull(interventionLifeRecords[0].Employee);

                Assert.AreEqual(65, interventionLifeRecords[0].RemainingLife);
                Assert.AreEqual(DateTime.Parse("24.04.2016 00:40:05"), interventionLifeRecords[0].DateOfVisit);
                Assert.AreEqual("Note", interventionLifeRecords[0].Notes);
            }
        }

        //[TestMethod]
        [TestCategory("InterventionLifeRecordRepository")]
        public void GetById_ShouldReturnInterventionLifeRecord()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var interventionLifeRecord = interventionLifeRecordRepository.GetById(dbContext, 1).FirstOrDefault();

                // assert
                Assert.AreEqual(1, interventionLifeRecord.Id);

                Assert.AreEqual(1, interventionLifeRecord.InterventionId);
                Assert.IsNotNull(interventionLifeRecord.Intervention);

                Assert.AreEqual(11, interventionLifeRecord.EmployeeId);
                Assert.IsNotNull(interventionLifeRecord.Employee);

                Assert.AreEqual(65, interventionLifeRecord.RemainingLife);
                Assert.AreEqual(DateTime.Parse("24.04.2016 00:40:05"), interventionLifeRecord.DateOfVisit);
                Assert.AreEqual("Note", interventionLifeRecord.Notes);
            }
        }

        //[TestMethod]
        [TestCategory("InterventionLifeRecordRepository")]
        public void SaveOrUpdate_ShouldUpdateExistingInterventionLifeRecord()
        {
            // arrange
            var interventionLifeRecord = new InterventionLifeRecord()
            {
                Id = 1,
                InterventionId = 1,
                EmployeeId = 12,
                RemainingLife = 40,
                DateOfVisit = DateTime.Now,
                Notes = "New Note"
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                interventionLifeRecordRepository.SaveOrUpdate(dbContext, interventionLifeRecord);
                dbContext.SaveChanges();
            }
        }

        //[TestMethod]
        [TestCategory("InterventionLifeRecordRepository")]
        public void SaveOrUpdate_ShouldInsertNewInterventionLifeRecord()
        {
            // arrange
            var interventionLifeRecord = new InterventionLifeRecord()
            {
                InterventionId = 1,
                EmployeeId = 17,
                RemainingLife = 25,
                DateOfVisit = DateTime.Now,
                Notes = "Note"
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                interventionLifeRecordRepository.SaveOrUpdate(dbContext, interventionLifeRecord);
                dbContext.SaveChanges();
            }
        }
    }
}
