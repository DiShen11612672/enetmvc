﻿using ENETMVC.Database;
using ENETMVC.Database.Models;
using ENETMVC.Database.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace ENETMVC.IMSTests.Database.Repositories
{
    [TestClass]
    public class TestInterventionStateRecordRepository
    {
        private InterventionStateRecordRepository interventionStateRecordRepository;

        [ClassInitialize]
        public static void SetupSuite(TestContext testContext)
        {
            // set path to database file
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\ENETMVC.Database\Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        [TestInitialize]
        public void Setup()
        {
            interventionStateRecordRepository = new InterventionStateRecordRepository();
        }

        //[TestMethod]
        [TestCategory("InterventionStateRecordRepository")]
        public void GetAll_ShouldReturnAllInterventionStateRecords()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var interventionStateRecords = interventionStateRecordRepository.GetAll(dbContext).ToList();

                // assert
                Assert.AreEqual(2, interventionStateRecords.Count);
                Assert.AreEqual(1, interventionStateRecords[0].Id);

                Assert.AreEqual(1, interventionStateRecords[0].InterventionId);
                Assert.IsNotNull(interventionStateRecords[0].Intervention);

                Assert.AreEqual(3, interventionStateRecords[0].EmployeeId);
                Assert.IsNotNull(interventionStateRecords[0].Employee);

                Assert.AreEqual(State.APPROVE, interventionStateRecords[0].State);
                Assert.AreEqual(DateTime.Parse("28.04.2016 10:59:49"), interventionStateRecords[0].Created);
            }
        }

        //[TestMethod]
        [TestCategory("InterventionStateRecordRepository")]
        public void GetById_ShouldReturnInterventionStateRecord()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var interventionStateRecord = interventionStateRecordRepository.GetById(dbContext, 1).FirstOrDefault();

                // assert
                Assert.AreEqual(1, interventionStateRecord.Id);

                Assert.AreEqual(1, interventionStateRecord.InterventionId);
                Assert.IsNotNull(interventionStateRecord.Intervention);

                Assert.AreEqual(3, interventionStateRecord.EmployeeId);
                Assert.IsNotNull(interventionStateRecord.Employee);

                Assert.AreEqual(State.APPROVE, interventionStateRecord.State);
                Assert.AreEqual(DateTime.Parse("28.04.2016 10:59:49"), interventionStateRecord.Created);
            }
        }

        //[TestMethod]
        [TestCategory("InterventionStateRecordRepository")]
        public void SaveOrUpdate_ShouldUpdateExistingInterventionStateRecord()
        {
            // arrange
            var interventionStateRecord = new InterventionStateRecord()
            {
                Id = 1,
                InterventionId = 1,
                EmployeeId = 4,
                State = State.COMPLETE,
                Created = DateTime.Now
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                interventionStateRecordRepository.SaveOrUpdate(dbContext, interventionStateRecord);
                dbContext.SaveChanges();
            }
        }

        //[TestMethod]
        [TestCategory("InterventionStateRecordRepository")]
        public void SaveOrUpdate_ShouldInsertNewInterventionStateRecord()
        {
            // arrange
            var interventionStateRecord = new InterventionStateRecord()
            {
                InterventionId = 1,
                EmployeeId = 3,
                State = State.CANCEL,
                Created = DateTime.Now
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                interventionStateRecordRepository.SaveOrUpdate(dbContext, interventionStateRecord);
                dbContext.SaveChanges();
            }
        }
    }
}
