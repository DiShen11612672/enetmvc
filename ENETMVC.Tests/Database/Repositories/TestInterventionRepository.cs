﻿using ENETMVC.Database;
using ENETMVC.Database.Models;
using ENETMVC.Database.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace ENETMVC.IMSTests.Database.Repositories
{
    [TestClass]
    public class TestInterventionRepository
    {
        private InterventionRepository interventionRepository;

        [ClassInitialize]
        public static void SetupSuite(TestContext testContext)
        {
            // set path to database file
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\ENETMVC.Database\Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        [TestInitialize]
        public void Setup()
        {
            interventionRepository = new InterventionRepository();
        }

        //[TestMethod]
        [TestCategory("InterventionRepository")]
        public void GetAll_ShouldReturnAllInterventions()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var interventions = interventionRepository.GetAll(dbContext).ToList();

                // assert
                Assert.AreEqual(6, interventions.Count);
                Assert.AreEqual(1, interventions[0].Id);

                Assert.AreEqual(9, interventions[0].EmployeeId);
                Assert.IsNotNull(interventions[0].Employee);

                Assert.AreEqual(2, interventions[0].ClientId);
                Assert.IsNotNull(interventions[0].Client);

                Assert.AreEqual(1, interventions[0].InterventionTypeId);
                Assert.IsNotNull(interventions[0].InterventionType);

                Assert.AreEqual(5, interventions[0].Duration);
                Assert.AreEqual(20, interventions[0].Costs);
                Assert.AreEqual("Note", interventions[0].Notes);
                Assert.AreEqual(DateTime.Parse("25.04.2016 12:33:17"), interventions[0].Created);
            }
        }

        //[TestMethod]
        [TestCategory("InterventionRepository")]
        public void GetById_ShouldReturnIntervention()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var intervention = interventionRepository.GetById(dbContext, 1).FirstOrDefault();

                // assert
                Assert.AreEqual(1, intervention.Id);

                Assert.AreEqual(9, intervention.EmployeeId);
                Assert.IsNotNull(intervention.Employee);

                Assert.AreEqual(2, intervention.ClientId);
                Assert.IsNotNull(intervention.Client);

                Assert.AreEqual(1, intervention.InterventionTypeId);
                Assert.IsNotNull(intervention.InterventionType);

                Assert.AreEqual(5, intervention.Duration);
                Assert.AreEqual(20, intervention.Costs);
                Assert.AreEqual("Note", intervention.Notes);
                Assert.AreEqual(DateTime.Parse("25.04.2016 12:33:17"), intervention.Created);
            }
        }

        //[TestMethod]
        [TestCategory("InterventionRepository")]
        public void SaveOrUpdate_ShouldUpdateExistingIntervention()
        {
            // arrange
            var intervention = new Intervention()
            {
                Id = 1,
                EmployeeId = 9,
                ClientId = 6,
                InterventionTypeId = 2,
                Duration = 1,
                Costs = 35,
                Notes = "New Note",
                Created = DateTime.Now
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                interventionRepository.SaveOrUpdate(dbContext, intervention);
                dbContext.SaveChanges();
            }
        }

        //[TestMethod]
        [TestCategory("InterventionRepository")]
        public void SaveOrUpdate_ShouldInsertNewIntervention()
        {
            // arrange
            var intervention = new Intervention()
            {
                EmployeeId = 11,
                ClientId = 3,
                InterventionTypeId = 1,
                Duration = 20,
                Costs = 350,
                Notes = "Note",
                Created = DateTime.Now
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                interventionRepository.SaveOrUpdate(dbContext, intervention);
                dbContext.SaveChanges();
            }
        }
    }
}
