﻿using ENETMVC.Database;
using ENETMVC.Database.Models;
using ENETMVC.Database.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace ENETMVC.IMSTests.Database.Repositories
{
    [TestClass]
    public class TestClientRepository
    {
        private ClientRepository clientRepository;

        [ClassInitialize]
        public static void SetupSuite(TestContext testContext)
        {
            // set path to database file
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\ENETMVC.Database\Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        [TestInitialize]
        public void Setup()
        {
            clientRepository = new ClientRepository();
        }

        //[TestMethod]
        [TestCategory("ClientRepository")]
        public void GetAll_ShouldReturnAllClients()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var clients = clientRepository.GetAll(dbContext).ToList();

                // assert
                Assert.AreEqual(7, clients.Count);
                Assert.AreEqual(2, clients[0].Id);
                Assert.AreEqual("Patrick Schedler", clients[0].Name);
                Assert.AreEqual("Darling Harbour", clients[0].Location);
                Assert.AreEqual(District.SYDNEY, clients[0].District);
                Assert.AreEqual(ClientType.FAMILY, clients[0].ClientType);
            }
        }

        //[TestMethod]
        [TestCategory("ClientRepository")]
        public void GetById_ShouldReturnClient()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var client = clientRepository.GetById(dbContext, 8).FirstOrDefault();

                // assert
                Assert.AreEqual(8, client.Id);
                Assert.AreEqual("Andy Shen", client.Name);
                Assert.AreEqual("Ultimo", client.Location);
                Assert.AreEqual(District.SYDNEY, client.District);
                Assert.AreEqual(ClientType.FAMILY, client.ClientType);
            }
        }

        //[TestMethod]
        [TestCategory("ClientRepository")]
        public void SaveOrUpdate_ShouldUpdateExistingClient()
        {
            // arrange
            var client = new Client()
            {
                Id = 8,
                Name = "Patrick Schedler",
                Location = "Ultimo",
                District = District.URBAN_INDONESIA,
                ClientType = ClientType.PERSON
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                clientRepository.SaveOrUpdate(dbContext, client);
                dbContext.SaveChanges();
            }
        }

        //[TestMethod]
        [TestCategory("ClientRepository")]
        public void SaveOrUpdate_ShouldInsertNewClient()
        {
            // arrange
            var client = new Client()
            {
                Name = "Andy Shen",
                Location = "Darling Harbour",
                District = District.URBAN_INDONESIA,
                ClientType = ClientType.COMMUNITY
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                clientRepository.SaveOrUpdate(dbContext, client);
                dbContext.SaveChanges();
            }
        }
    }
}
