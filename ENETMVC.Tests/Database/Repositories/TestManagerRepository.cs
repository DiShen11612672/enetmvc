﻿using ENETMVC.Database;
using ENETMVC.Database.Models;
using ENETMVC.Database.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace ENETMVC.IMSTests.Database.Repositories
{
    [TestClass]
    public class TestManagerRepository
    {
        private ManagerRepository managerRepository;

        [ClassInitialize]
        public static void SetupSuite(TestContext testContext)
        {
            // set path to database file
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\ENETMVC.Database\Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        [TestInitialize]
        public void Setup()
        {
            managerRepository = new ManagerRepository();
        }

        //[TestMethod]
        [TestCategory("ManagerRepository")]
        public void GetAll_ShouldReturnAllManagers()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var managers = managerRepository.GetAll(dbContext).ToList();

                // assert
                Assert.AreEqual(1, managers.Count);
                Assert.AreEqual(1, managers[0].Id);
                Assert.AreEqual("George", managers[0].Name);
                Assert.AreEqual("0c5bfb3d-f37d-4e8b-8407-85c27b256b33", managers[0].UserId);
                Assert.AreEqual(District.SYDNEY, managers[0].District);
                Assert.AreEqual(500, managers[0].MaxDuration);
                Assert.AreEqual(2000, managers[0].MaxCosts);
            }
        }

        //[TestMethod]
        [TestCategory("ManagerRepository")]
        public void GetById_ShouldReturnManager()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var manager = managerRepository.GetById(dbContext, 1).FirstOrDefault();

                // assert
                Assert.AreEqual(1, manager.Id);
                Assert.AreEqual("George", manager.Name);
                Assert.AreEqual("0c5bfb3d-f37d-4e8b-8407-85c27b256b33", manager.UserId);
                Assert.AreEqual(District.SYDNEY, manager.District);
                Assert.AreEqual(500, manager.MaxDuration);
                Assert.AreEqual(2000, manager.MaxCosts);
            }
        }

        //[TestMethod]
        [TestCategory("ManagerRepository")]
        public void SaveOrUpdate_ShouldUpdateExistingManager()
        {
            // arrange
            var manager = new Manager()
            {
                Id = 1,
                Name = "George",
                UserId = "0c5bfb3d-f37d-4e8b-8407-85c27b256b33",
                District = District.RURAL_NSW,
                MaxDuration = 500,
                MaxCosts = 100
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                managerRepository.SaveOrUpdate(dbContext, manager);
                dbContext.SaveChanges();
            }
        }

        //[TestMethod]
        [TestCategory("ManagerRepository")]
        public void SaveOrUpdate_ShouldInsertNewManager()
        {
            // arrange
            var manager = new Manager()
            {
                Name = "Wai Lok Yuen",
                UserId = "8c6c7238-1a63-4f7d-adf3-b7ae9d40987c",
                District = District.SYDNEY,
                MaxDuration = 2000,
                MaxCosts = 1500
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                managerRepository.SaveOrUpdate(dbContext, manager);
                dbContext.SaveChanges();
            }
        }
    }
}
