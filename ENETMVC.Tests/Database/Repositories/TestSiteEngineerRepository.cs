﻿using ENETMVC.Database;
using ENETMVC.Database.Models;
using ENETMVC.Database.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace ENETMVC.IMSTests.Database.Repositories
{
    [TestClass]
    public class TestSiteEngineerRepository
    {
        private SiteEngineerRepository siteEngineerRepository;

        [ClassInitialize]
        public static void SetupSuite(TestContext testContext)
        {
            // set path to database file
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\ENETMVC.Database\Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        [TestInitialize]
        public void Setup()
        {
            siteEngineerRepository = new SiteEngineerRepository();
        }

        //[TestMethod]
        [TestCategory("SiteEngineerRepository")]
        public void GetAll_ShouldReturnAllSiteEngineers()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var siteEngineers = siteEngineerRepository.GetAll(dbContext).ToList();

                // assert
                Assert.AreEqual(6, siteEngineers.Count);
                Assert.AreEqual(3, siteEngineers[0].Id);
                Assert.AreEqual("Lisa", siteEngineers[0].Name);
                Assert.AreEqual("fa6d10e5-a80e-4cb9-a03c-ddfb1d648117", siteEngineers[0].UserId);
                Assert.AreEqual(District.SYDNEY, siteEngineers[0].District);
                Assert.AreEqual(200, siteEngineers[0].MaxDuration);
                Assert.AreEqual(1500, siteEngineers[0].MaxCosts);
            }
        }

        //[TestMethod]
        [TestCategory("SiteEngineerRepository")]
        public void GetById_ShouldReturnSiteEngineer()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var siteEngineer = siteEngineerRepository.GetById(dbContext, 3).FirstOrDefault();

                // assert
                Assert.AreEqual(3, siteEngineer.Id);
                Assert.AreEqual("Lisa", siteEngineer.Name);
                Assert.AreEqual("fa6d10e5-a80e-4cb9-a03c-ddfb1d648117", siteEngineer.UserId);
                Assert.AreEqual(District.SYDNEY, siteEngineer.District);
                Assert.AreEqual(200, siteEngineer.MaxDuration);
                Assert.AreEqual(1500, siteEngineer.MaxCosts);
            }
        }

        //[TestMethod]
        [TestCategory("SiteEngineerRepository")]
        public void SaveOrUpdate_ShouldUpdateExistingSiteEngineer()
        {
            // arrange
            var siteEngineer = new SiteEngineer()
            {
                Id = 3,
                Name = "Lisa",
                UserId = "fa6d10e5-a80e-4cb9-a03c-ddfb1d648117",
                District = District.RURAL_NSW,
                MaxDuration = 500,
                MaxCosts = 100
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                siteEngineerRepository.SaveOrUpdate(dbContext, siteEngineer);
                dbContext.SaveChanges();
            }
        }

        //[TestMethod]
        [TestCategory("SiteEngineerRepository")]
        public void SaveOrUpdate_ShouldInsertNewSiteEngineer()
        {
            // arrange
            var siteEngineer = new SiteEngineer()
            {
                Name = "Wai Lok Yuen",
                UserId = "8c6c7238-1a63-4f7d-adf3-b7ae9d40987c",
                District = District.SYDNEY,
                MaxDuration = 2000,
                MaxCosts = 1500
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                siteEngineerRepository.SaveOrUpdate(dbContext, siteEngineer);
                dbContext.SaveChanges();
            }
        }
    }
}
