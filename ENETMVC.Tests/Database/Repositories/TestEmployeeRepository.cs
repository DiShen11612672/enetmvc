﻿using ENETMVC.Database;
using ENETMVC.Database.Models;
using ENETMVC.Database.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace ENETMVC.IMSTests.Database.Repositories
{
    [TestClass]
    public class TestEmployeeRepository
    {
        private EmployeeRepository employeeRepository;

        [ClassInitialize]
        public static void SetupSuite(TestContext testContext)
        {
            // set path to database file
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\ENETMVC.Database\Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        [TestInitialize]
        public void Setup()
        {
            employeeRepository = new EmployeeRepository();
        }

        //[TestMethod]
        [TestCategory("EmployeeRepository")]
        public void GetAll_ShouldReturnAllEmployees()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var employees = employeeRepository.GetAll(dbContext).ToList();

                // assert
                Assert.AreEqual(8, employees.Count);
                Assert.AreEqual(1, employees[0].Id);
                Assert.AreEqual("George", employees[0].Name);
                Assert.AreEqual("0c5bfb3d-f37d-4e8b-8407-85c27b256b33", employees[0].UserId);
                Assert.AreEqual(District.SYDNEY, ((Manager)employees[0]).District);
                Assert.AreEqual(500, ((Manager)employees[0]).MaxDuration);
                Assert.AreEqual(2000, ((Manager)employees[0]).MaxCosts);
            }
        }

        //[TestMethod]
        [TestCategory("EmployeeRepository")]
        public void GetById_ShouldReturnEmployee()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var employee = employeeRepository.GetById(dbContext, 17).FirstOrDefault();

                // assert
                Assert.AreEqual(17, employee.Id);
                Assert.AreEqual("Ben Stevens", employee.Name);
                Assert.AreEqual("fbf49f00-582a-430f-8d89-b877bec8cd88", employee.UserId);
                Assert.AreEqual(District.RURAL_NSW, ((SiteEngineer)employee).District);
                Assert.AreEqual(500, ((SiteEngineer)employee).MaxDuration);
                Assert.AreEqual(100, ((SiteEngineer)employee).MaxCosts);
            }
        }

        //[TestMethod]
        [TestCategory("EmployeeRepository")]
        public void SaveOrUpdate_ShouldUpdateExistingEmployee()
        {
            // arrange
            var employee = new SiteEngineer()
            {
                Id = 17,
                Name = "Andy Shen",
                UserId = "fbf49f00-582a-430f-8d89-b877bec8cd88",
                District = District.URBAN_INDONESIA,
                MaxDuration = 1000,
                MaxCosts = 200
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                employeeRepository.SaveOrUpdate(dbContext, employee);
                dbContext.SaveChanges();
            }
        }

        //[TestMethod]
        [TestCategory("EmployeeRepository")]
        public void SaveOrUpdate_ShouldInsertNewEmployee()
        {
            // arrange
            var employee = new SiteEngineer()
            {
                Name = "Wai Lok Yuen",
                UserId = "fbf49f00-582a-430f-8d89-b877bec8cd88",
                District = District.SYDNEY,
                MaxDuration = 2000,
                MaxCosts = 1500
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                employeeRepository.SaveOrUpdate(dbContext, employee);
                dbContext.SaveChanges();
            }
        }
    }
}
