﻿using ENETMVC.Database;
using ENETMVC.Database.Models;
using ENETMVC.Database.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace ENETMVC.IMSTests.Database.Repositories
{
    [TestClass]
    public class TestInterventionTypeRepository
    {
        private InterventionTypeRepository interventionTypeRepository;

        [ClassInitialize]
        public static void SetupSuite(TestContext testContext)
        {
            // set path to database file
            string path = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\ENETMVC.Database\Data"));
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        [TestInitialize]
        public void Setup()
        {
            interventionTypeRepository = new InterventionTypeRepository();
        }

        //[TestMethod]
        [TestCategory("InterventionTypeRepository")]
        public void GetAll_ShouldReturnAllInterventionTypes()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var interventionTypes = interventionTypeRepository.GetAll(dbContext).ToList();

                // assert
                Assert.AreEqual(3, interventionTypes.Count);
                Assert.AreEqual(1, interventionTypes[0].Id);
                Assert.AreEqual("Portable Toilet", interventionTypes[0].Name);
                Assert.AreEqual(1, interventionTypes[0].Duration);
                Assert.AreEqual(65, interventionTypes[0].Costs);
            }
        }

        //[TestMethod]
        [TestCategory("InterventionTypeRepository")]
        public void GetById_ShouldReturnInterventionType()
        {
            // arrange
            using (var dbContext = new IMSDbContext())
            {
                // act
                var interventionType = interventionTypeRepository.GetById(dbContext, 1).FirstOrDefault();

                // assert
                Assert.AreEqual(1, interventionType.Id);
                Assert.AreEqual("Portable Toilet", interventionType.Name);
                Assert.AreEqual(1, interventionType.Duration);
                Assert.AreEqual(65, interventionType.Costs);
            }
        }

        //[TestMethod]
        [TestCategory("InterventionTypeRepository")]
        public void SaveOrUpdate_ShouldUpdateExistingInterventionType()
        {
            // arrange
            var interventionType = new InterventionType()
            {
                Id = 1,
                Name = "Portable Toilet",
                Duration = 3,
                Costs = 90
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                interventionTypeRepository.SaveOrUpdate(dbContext, interventionType);
                dbContext.SaveChanges();
            }
        }

        //[TestMethod]
        [TestCategory("InterventionTypeRepository")]
        public void SaveOrUpdate_ShouldInsertNewInterventionType()
        {
            // arrange
            var interventionType = new InterventionType()
            {
                Name = "First Aid Kit",
                Duration = 0.5M,
                Costs = 30
            };

            using (var dbContext = new IMSDbContext())
            {
                // act
                interventionTypeRepository.SaveOrUpdate(dbContext, interventionType);
                dbContext.SaveChanges();
            }
        }
    }
}
