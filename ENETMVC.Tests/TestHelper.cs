﻿using MvcContrib.TestHelper.Fakes;
using Rhino.Mocks;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ENETMVC.IMSTests
{
    public static class TestHelper
    {
        public static ControllerContext MockControllerContext(Controller controller)
        {
            var httpContext = MockRepository.GenerateMock<HttpContextBase>();
            var httpRequest = MockRepository.GenerateMock<HttpRequestBase>();
            httpContext.Stub(x => x.Request).Return(httpRequest);
            return new ControllerContext(httpContext, new RouteData(), controller);
        }

        public static ControllerContext WithAuthenticatedUser(this ControllerContext context, string id, string userName)
        {
            return WithAuthenticatedUser(context, id, userName, null);
        }

        public static ControllerContext WithAuthenticatedUser(this ControllerContext context, string id, string userName, string[] roles)
        {
            var user = new FakePrincipal(new FakeClaimIdentity(id, userName), roles);
            context.HttpContext.Stub(x => x.User).Return(user);
            return new ControllerContext(context.HttpContext, new RouteData(), context.Controller);
        }

        public static ControllerContext WithNonAuthenticatedUser(this ControllerContext context)
        {
            var user = new FakePrincipal(new FakeClaimIdentity(String.Empty, String.Empty), null);
            context.HttpContext.Stub(x => x.User).Return(user);
            return new ControllerContext(context.HttpContext, new RouteData(), context.Controller);
        }
    }
}
