﻿using MvcContrib.TestHelper.Fakes;
using System.Collections.Generic;
using System.Security.Claims;

namespace ENETMVC.IMSTests
{
    public class FakeClaimIdentity : ClaimsIdentity
    {

        private readonly FakeIdentity fakeIdentity;


        public FakeClaimIdentity(string id, string userName) : base(userName)
        {
            fakeIdentity = new FakeIdentity(userName);
            base.AddClaims(new List<Claim>
            {
                new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", userName),
                new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier", id)
            });
        }

        public override bool IsAuthenticated
        {
            get { return fakeIdentity.IsAuthenticated; }
        }

        public override string Name
        {
            get { return fakeIdentity.Name; }
        }

        public override string AuthenticationType
        {
            get
            {
                return fakeIdentity.AuthenticationType;
            }
        }
    }
}
