﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ENETMVC.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using ENETMVC.Models;
using ENETMVC.Database.Models;
using ENETMVC.Database.Interfaces;
using Rhino.Mocks;
using ENETMVC.IMSTests;

namespace ENETMVC.Controllers.Tests
{
    [TestClass()]
    public class TestEmployeeController
    {
        private IDatabaseFacade dbFacadeMock;
        private IDatabaseFacadeFactory dbFacadeFactoryMock;
        private SiteEngineer testSite;
        private EmployeeController employeeController;

        [TestInitialize]
        public void Setup()
        {

            dbFacadeMock = MockRepository.GenerateMock<IDatabaseFacade>();
            dbFacadeFactoryMock = MockRepository.GenerateMock<IDatabaseFacadeFactory>();
            dbFacadeFactoryMock.Stub(dbFacadeFactory => dbFacadeFactory.Create())
                .Return(dbFacadeMock);

            employeeController = new EmployeeController(dbFacadeFactoryMock);

            var userId = Guid.NewGuid().ToString();
            var userName = "user";

            testSite = new SiteEngineer()
            {
                Id = 3,
                UserId = userId,
                Name = "Employee101",
                District = District.SYDNEY,
                MaxDuration = 10,
                MaxCosts = 500
            };

            employeeController.ControllerContext = TestHelper.MockControllerContext(employeeController).WithAuthenticatedUser(userId, userName);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetById<SiteEngineer>(testSite.Id))
                .Return(testSite);
        }

        [TestMethod]
        [TestCategory("EmployeeController")]
        public void TestIndexView()
        {
            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetAll<SiteEngineer>())
                .Return(new List<SiteEngineer>());

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetAll<Manager>())
                .Return(new List<Manager>());

            //Test if the index view contains the correct view model;
            var result = employeeController.Index() as ViewResult;
            Assert.IsInstanceOfType(result.Model, typeof(AccountantViewModel));
            Assert.AreEqual(((AccountantViewModel)result.Model).SiteEngineers.Count, 0);
            Assert.AreEqual(((AccountantViewModel)result.Model).Managers.Count, 0);
        }

        [TestMethod]
        [TestCategory("EmployeeController")]
        public void ListItemVerify()
        {
            //Test to make sure the selectlist contains correct items
            int counter = 0;
            var controller = new ReportController();
            List<SelectListItem> c = controller.getItems();
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = District.RURAL_INDONESIA.ToString(), Value = "0" });
            items.Add(new SelectListItem { Text = District.RURAL_NSW.ToString(), Value = "1" });
            items.Add(new SelectListItem { Text = District.RURAL_PNG.ToString(), Value = "2" });
            items.Add(new SelectListItem { Text = District.SYDNEY.ToString(), Value = "3" });
            items.Add(new SelectListItem { Text = District.URBAN_INDONESIA.ToString(), Value = "4" });
            items.Add(new SelectListItem { Text = District.URBAN_PNG.ToString(), Value = "5" });
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].Text.Equals(c[i].Text) && items[i].Value.Equals(c[i].Value))
                    counter++;
            }
            Assert.AreEqual(6, counter);
        }

        [TestMethod]
        [TestCategory("EmployeeController")]
        public void ChangeDistrictTestInvalidId()
        {
            //verify if the change district action will return http not found
            var result = employeeController.ChangeDistrict(5125125);
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult));
        }

        [TestMethod]
        [TestCategory("EmployeeController")]
        public void ChangeDistrictTestvalidId()
        {
            //test if the change district action will return valid viewresult
            var result = employeeController.ChangeDistrict(testSite.Id);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        [TestCategory("EmployeeController")]
        public void ChangeDistrictActualwork()
        {
            //test if the change district action can change the view model employee's district
            District d = District.RURAL_PNG;
            var result = (ViewResult)employeeController.ChangeDistrict(testSite.Id, d);
            var model = (EmployeeViewModel)result.Model;
            Assert.AreEqual(d, model.Employee.District);
        }

    }
}