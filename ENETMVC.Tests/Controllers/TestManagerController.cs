﻿using ENETMVC.Controllers;
using ENETMVC.Database.Interfaces;
using ENETMVC.Database.Models;
using ENETMVC.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace ENETMVC.IMSTests.Controllers
{
    [TestClass]
    public class TestManagerController
    {
        //// mocks
        //private IDatabaseFacade dbFacadeMock;
        //private IDatabaseFacadeFactory dbFacadeFactoryMock;

        //// Controller to test.
        //private ManagerController manCon;

        //// User Data
        //// user[0] - user ID
        //// user[1] - username
        //private string[] siteEngUser = new string[2];
        //private string[] managerUser = new string[2];

        //// DB model objects.
        //private SiteEngineer siteEng;
        //private Manager manager;
        //private Intervention intervention;
        //private InterventionType interventionType;
        //private Client client;

        //// Private methods.

        ///// <summary>
        ///// Initialise mock objects to perform tests.
        ///// </summary>
        //private void initialiseModels()
        //{
        //    siteEngUser[0] = Guid.NewGuid().ToString();
        //    siteEngUser[1] = "seUser";

        //    managerUser[0] = Guid.NewGuid().ToString();
        //    managerUser[1] = "manUser";

        //    siteEng = new SiteEngineer()
        //    {
        //        Id = 1,
        //        UserId = siteEngUser[0],
        //        Name = "Fred Nerk",
        //        District = District.SYDNEY,
        //        MaxDuration = 10,
        //        MaxCosts = 500
        //    };

        //    manager = new Manager()
        //    {
        //        Id = 2,
        //        UserId = managerUser[0],
        //        Name = "Mary Poppins",
        //        District = District.SYDNEY,
        //        MaxDuration = 100,
        //        MaxCosts = 5000
        //    };

        //    intervention = new Intervention()
        //    {
        //        Id = 1,
        //        Duration = 2,
        //        Costs = 100,
        //        Notes = "Note",
        //        Created = DateTime.Now
        //    };

        //    interventionType = new InterventionType()
        //    {
        //        Id = 1,
        //        Name = "InterventionType",
        //        Duration = 2,
        //        Costs = 100
        //    };

        //    client = new Client()
        //    {
        //        Id = 1,
        //        Name = "Client",
        //        District = District.SYDNEY,
        //        ClientType = ClientType.PERSON,
        //        Location = "Location"
        //    };

        //    // set model object references
        //    manager.ProposedInterventions.Add(intervention);
        //    intervention.Client = client;
        //    intervention.Employee = siteEng;
        //    intervention.InterventionType = interventionType;
        //    interventionType.Interventions = new List<Intervention>() { intervention };
        //    client.Interventions = new List<Intervention>() { intervention };

        //    // configure manager mocks
        //    manCon.ControllerContext = TestHelper.MockControllerContext(manCon).WithAuthenticatedUser(managerUser[0], managerUser[1]);
        //    dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetEmployeeByUserId(managerUser[0])).Return(manager);
        //}

        //[TestInitialize]
        //public void Setup()
        //{
        //    dbFacadeMock = MockRepository.GenerateMock<IDatabaseFacade>();
        //    dbFacadeFactoryMock = MockRepository.GenerateMock<IDatabaseFacadeFactory>();
        //    dbFacadeFactoryMock.Stub(dbFacadeFactory => dbFacadeFactory.Create())
        //        .Return(dbFacadeMock);

        //    manCon = new ManagerController(dbFacadeFactoryMock);
        //}

        //[TestMethod]
        //[TestCategory("ManagerController")]
        //public void InterventionLifeRecord_NonExistingIntervention_ShouldReturnNotFound()
        //{
        //    // arrange
        //    initialiseModels();

        //    // configure intervention mock
        //    dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetById<Intervention>(intervention.Id)).Return(null);

        //    // act
        //    var result = (HttpStatusCodeResult)ManagerController.ProposedInterventions.(intervention.Id);

        //    // assert
        //    Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        //}

        //[TestMethod]
        //[TestCategory("ManagerController")]
        //public void InterventionLifeRecord_ExistingIntervention_ShouldReturnView()
        //{
        //    // arrange
        //    var userId = Guid.NewGuid().ToString();
        //    var userName = "user";

        //    var employee = new SiteEngineer()
        //    {
        //        Id = 1,
        //        UserId = userId,
        //        Name = "Employee",
        //        District = District.SYDNEY,
        //        MaxDuration = 10,
        //        MaxCosts = 500
        //    };

        //    var intervention = new Intervention()
        //    {
        //        Id = 1,
        //        Duration = 2,
        //        Costs = 100,
        //        Notes = "Note",
        //        Created = DateTime.Now
        //    };

        //    var interventionType = new InterventionType()
        //    {
        //        Id = 1,
        //        Name = "InterventionType",
        //        Duration = 2,
        //        Costs = 100
        //    };

        //    var client = new Client()
        //    {
        //        Id = 1,
        //        Name = "Client",
        //        District = District.SYDNEY,
        //        ClientType = ClientType.PERSON,
        //        Location = "Location"
        //    };

        //    // set model object references
        //    employee.Interventions = new List<Intervention>() { intervention };
        //    intervention.Client = client;
        //    intervention.Employee = employee;
        //    intervention.InterventionType = interventionType;
        //    interventionType.Interventions = new List<Intervention>() { intervention };
        //    client.Interventions = new List<Intervention>() { intervention };

        //    // configure mocks
        //    ManagerController.ControllerContext = TestHelper.MockControllerContext(ManagerController).WithAuthenticatedUser(userId, userName);

        //    dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetEmployeeByUserId(userId))
        //        .Return(employee);

        //    dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetById<Intervention>(intervention.Id))
        //        .Return(intervention);

        //    // act
        //    var result = (ViewResult)ManagerController.InterventionLifeRecord(intervention.Id);
        //    var viewModel = (InterventionLifeRecordViewModel)result.Model;

        //    // assert
        //    Assert.AreEqual(employee.Name, viewModel.Employee);
        //    Assert.IsNotNull(viewModel.DateOfVisit);
        //    Assert.AreEqual(default(decimal), viewModel.RemainingLife);
        //    Assert.AreEqual(default(string), viewModel.Notes);
        //}

        //[TestMethod]
        //[TestCategory("ManagerController")]
        //public void InterventionStateRecord_NonExistingIntervention_ShouldReturnNotFound()
        //{
        //    // arrange
        //    var userId = Guid.NewGuid().ToString();
        //    var userName = "user";

        //    var employee = new SiteEngineer()
        //    {
        //        Id = 1,
        //        UserId = userId,
        //        Name = "Employee",
        //        District = District.SYDNEY,
        //        MaxDuration = 10,
        //        MaxCosts = 500
        //    };

        //    var intervention = new Intervention()
        //    {
        //        Id = 1,
        //        Duration = 2,
        //        Costs = 100,
        //        Notes = "Note",
        //        Created = DateTime.Now
        //    };

        //    var interventionType = new InterventionType()
        //    {
        //        Id = 1,
        //        Name = "InterventionType",
        //        Duration = 2,
        //        Costs = 100
        //    };

        //    var client = new Client()
        //    {
        //        Id = 1,
        //        Name = "Client",
        //        District = District.SYDNEY,
        //        ClientType = ClientType.PERSON,
        //        Location = "Location"
        //    };

        //    // set model object references
        //    employee.Interventions = new List<Intervention>() { intervention };
        //    intervention.Client = client;
        //    intervention.Employee = employee;
        //    intervention.InterventionType = interventionType;
        //    interventionType.Interventions = new List<Intervention>() { intervention };
        //    client.Interventions = new List<Intervention>() { intervention };

        //    // configure mocks
        //    ManagerController.ControllerContext = TestHelper.MockControllerContext(ManagerController).WithAuthenticatedUser(userId, userName);

        //    dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetEmployeeByUserId(userId))
        //        .Return(employee);

        //    dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetById<Intervention>(intervention.Id))
        //        .Return(null);

        //    // act
        //    var result = (HttpStatusCodeResult)ManagerController.InterventionStateRecord(intervention.Id);

        //    // assert
        //    Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        //}

        //[TestMethod]
        //[TestCategory("ManagerController")]
        //public void InterventionStateRecord_ExistingIntervention_ShouldReturnView()
        //{
        //    // arrange
        //    var userId = Guid.NewGuid().ToString();
        //    var userName = "user";

        //    var employee = new SiteEngineer()
        //    {
        //        Id = 1,
        //        UserId = userId,
        //        Name = "Employee",
        //        District = District.SYDNEY,
        //        MaxDuration = 10,
        //        MaxCosts = 500
        //    };

        //    var intervention = new Intervention()
        //    {
        //        Id = 1,
        //        Duration = 2,
        //        Costs = 100,
        //        Notes = "Note",
        //        Created = DateTime.Now
        //    };

        //    var interventionType = new InterventionType()
        //    {
        //        Id = 1,
        //        Name = "InterventionType",
        //        Duration = 2,
        //        Costs = 100
        //    };

        //    var client = new Client()
        //    {
        //        Id = 1,
        //        Name = "Client",
        //        District = District.SYDNEY,
        //        ClientType = ClientType.PERSON,
        //        Location = "Location"
        //    };

        //    // set model object references
        //    employee.Interventions = new List<Intervention>() { intervention };
        //    intervention.Client = client;
        //    intervention.Employee = employee;
        //    intervention.InterventionType = interventionType;
        //    interventionType.Interventions = new List<Intervention>() { intervention };
        //    client.Interventions = new List<Intervention>() { intervention };

        //    // configure mocks
        //    ManagerController.ControllerContext = TestHelper.MockControllerContext(ManagerController).WithAuthenticatedUser(userId, userName);

        //    dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetEmployeeByUserId(userId))
        //        .Return(employee);

        //    dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetById<Intervention>(intervention.Id))
        //        .Return(intervention);

        //    // act
        //    var result = (ViewResult)ManagerController.InterventionStateRecord(intervention.Id);
        //    var viewModel = (InterventionStateRecordViewModel)result.Model;

        //    // assert
        //    Assert.AreEqual(employee.Name, viewModel.Employee);
        //    Assert.AreEqual(State.APPROVE.ToString(), viewModel.PossibleStates.Skip(0).First().Text);
        //    Assert.AreEqual(State.CANCEL.ToString(), viewModel.PossibleStates.Skip(1).First().Text);
        //    Assert.IsNotNull(viewModel.CreationDate);
        //    Assert.AreEqual(default(string), viewModel.SelectedState);
        //}
    }
}
