﻿using ENETMVC.Controllers;
using ENETMVC.Database.Interfaces;
using ENETMVC.Database.Models;
using ENETMVC.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;

namespace ENETMVC.IMSTests.Controllers
{
    [TestClass]
    public class TestInterventionController
    {
        // mocks
        private IDatabaseFacade dbFacadeMock;
        private IDatabaseFacadeFactory dbFacadeFactoryMock;

        private InterventionController interventionController;

        [TestInitialize]
        public void Setup()
        {
            dbFacadeMock = MockRepository.GenerateMock<IDatabaseFacade>();
            dbFacadeFactoryMock = MockRepository.GenerateMock<IDatabaseFacadeFactory>();
            dbFacadeFactoryMock.Stub(dbFacadeFactory => dbFacadeFactory.Create())
                .Return(dbFacadeMock);

            interventionController = new InterventionController(dbFacadeFactoryMock);
        }

        [TestMethod]
        [TestCategory("InterventionController")]
        public void InterventionLifeRecord_NonExistingIntervention_ShouldReturnNotFound()
        {
            // arrange
            var userId = Guid.NewGuid().ToString();
            var userName = "user";

            var employee = new SiteEngineer()
            {
                Id = 1,
                UserId = userId,
                Name = "Employee",
                District = District.SYDNEY,
                MaxDuration = 10,
                MaxCosts = 500
            };

            var intervention = new Intervention()
            {
                Id = 1,
                Duration = 2,
                Costs = 100,
                Notes = "Note",
                Created = DateTime.Now
            };

            var interventionType = new InterventionType()
            {
                Id = 1,
                Name = "InterventionType",
                Duration = 2,
                Costs = 100
            };

            var client = new Client()
            {
                Id = 1,
                Name = "Client",
                District = District.SYDNEY,
                ClientType = ClientType.PERSON,
                Location = "Location"
            };

            // set model object references
            employee.Interventions = new List<Intervention>() { intervention };
            intervention.Client = client;
            intervention.Employee = employee;
            intervention.InterventionType = interventionType;
            interventionType.Interventions = new List<Intervention>() { intervention };
            client.Interventions = new List<Intervention>() { intervention };

            // configure mocks
            interventionController.ControllerContext = TestHelper.MockControllerContext(interventionController).WithAuthenticatedUser(userId, userName);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetEmployeeByUserId(userId))
                .Return(employee);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetById<Intervention>(intervention.Id))
                .Return(null);

            // act
            var result = (HttpStatusCodeResult)interventionController.InterventionLifeRecord(intervention.Id);

            // assert
            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        [TestCategory("InterventionController")]
        public void InterventionLifeRecord_ExistingIntervention_ShouldReturnView()
        {
            // arrange
            var userId = Guid.NewGuid().ToString();
            var userName = "user";

            var employee = new SiteEngineer()
            {
                Id = 1,
                UserId = userId,
                Name = "Employee",
                District = District.SYDNEY,
                MaxDuration = 10,
                MaxCosts = 500
            };

            var intervention = new Intervention()
            {
                Id = 1,
                Duration = 2,
                Costs = 100,
                Notes = "Note",
                Created = DateTime.Now
            };

            var interventionType = new InterventionType()
            {
                Id = 1,
                Name = "InterventionType",
                Duration = 2,
                Costs = 100
            };

            var client = new Client()
            {
                Id = 1,
                Name = "Client",
                District = District.SYDNEY,
                ClientType = ClientType.PERSON,
                Location = "Location"
            };

            // set model object references
            employee.Interventions = new List<Intervention>() { intervention };
            intervention.Client = client;
            intervention.Employee = employee;
            intervention.InterventionType = interventionType;
            interventionType.Interventions = new List<Intervention>() { intervention };
            client.Interventions = new List<Intervention>() { intervention };

            // configure mocks
            interventionController.ControllerContext = TestHelper.MockControllerContext(interventionController).WithAuthenticatedUser(userId, userName);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetEmployeeByUserId(userId))
                .Return(employee);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetById<Intervention>(intervention.Id))
                .Return(intervention);

            // act
            var result = (ViewResult)interventionController.InterventionLifeRecord(intervention.Id);
            var viewModel = (InterventionLifeRecordViewModel)result.Model;

            // assert
            Assert.AreEqual(employee.Name, viewModel.Employee);
            Assert.IsNotNull(viewModel.DateOfVisit);
            Assert.AreEqual(default(decimal), viewModel.RemainingLife);
            Assert.AreEqual(default(string), viewModel.Notes);
        }

        [TestMethod]
        [TestCategory("InterventionController")]
        public void InterventionStateRecord_NonExistingIntervention_ShouldReturnNotFound()
        {
            // arrange
            var userId = Guid.NewGuid().ToString();
            var userName = "user";

            var employee = new SiteEngineer()
            {
                Id = 1,
                UserId = userId,
                Name = "Employee",
                District = District.SYDNEY,
                MaxDuration = 10,
                MaxCosts = 500
            };

            var intervention = new Intervention()
            {
                Id = 1,
                Duration = 2,
                Costs = 100,
                Notes = "Note",
                Created = DateTime.Now
            };

            var interventionType = new InterventionType()
            {
                Id = 1,
                Name = "InterventionType",
                Duration = 2,
                Costs = 100
            };

            var client = new Client()
            {
                Id = 1,
                Name = "Client",
                District = District.SYDNEY,
                ClientType = ClientType.PERSON,
                Location = "Location"
            };

            // set model object references
            employee.Interventions = new List<Intervention>() { intervention };
            intervention.Client = client;
            intervention.Employee = employee;
            intervention.InterventionType = interventionType;
            interventionType.Interventions = new List<Intervention>() { intervention };
            client.Interventions = new List<Intervention>() { intervention };

            // configure mocks
            interventionController.ControllerContext = TestHelper.MockControllerContext(interventionController).WithAuthenticatedUser(userId, userName);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetEmployeeByUserId(userId))
                .Return(employee);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetById<Intervention>(intervention.Id))
                .Return(null);

            // act
            var result = (HttpStatusCodeResult)interventionController.InterventionStateRecord(intervention.Id);

            // assert
            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        [TestCategory("InterventionController")]
        public void InterventionStateRecord_ExistingIntervention_ShouldReturnView()
        {
            // arrange
            var userId = Guid.NewGuid().ToString();
            var userName = "user";

            var employee = new SiteEngineer()
            {
                Id = 1,
                UserId = userId,
                Name = "Employee",
                District = District.SYDNEY,
                MaxDuration = 10,
                MaxCosts = 500
            };

            var intervention = new Intervention()
            {
                Id = 1,
                Duration = 2,
                Costs = 100,
                Notes = "Note",
                Created = DateTime.Now
            };

            var interventionType = new InterventionType()
            {
                Id = 1,
                Name = "InterventionType",
                Duration = 2,
                Costs = 100
            };

            var client = new Client()
            {
                Id = 1,
                Name = "Client",
                District = District.SYDNEY,
                ClientType = ClientType.PERSON,
                Location = "Location"
            };

            // set model object references
            employee.Interventions = new List<Intervention>() { intervention };
            intervention.Client = client;
            intervention.Employee = employee;
            intervention.InterventionType = interventionType;
            interventionType.Interventions = new List<Intervention>() { intervention };
            client.Interventions = new List<Intervention>() { intervention };

            // configure mocks
            interventionController.ControllerContext = TestHelper.MockControllerContext(interventionController).WithAuthenticatedUser(userId, userName);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetEmployeeByUserId(userId))
                .Return(employee);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetById<Intervention>(intervention.Id))
                .Return(intervention);

            // act
            var result = (ViewResult)interventionController.InterventionStateRecord(intervention.Id);
            var viewModel = (InterventionStateRecordViewModel)result.Model;

            // assert
            Assert.AreEqual(employee.Name, viewModel.Employee);
            Assert.AreEqual(State.APPROVE.ToString(), viewModel.PossibleStates.Skip(0).First().Text);
            Assert.AreEqual(State.CANCEL.ToString(), viewModel.PossibleStates.Skip(1).First().Text);
            Assert.IsNotNull(viewModel.CreationDate);
            Assert.AreEqual(default(string), viewModel.SelectedState);
        }
    }
}
