﻿using ENETMVC.Controllers;
using ENETMVC.Database.Interfaces;
using ENETMVC.Database.Models;
using ENETMVC.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using ENETMVC.IMSTests;

namespace ENETMVC.Tests.Controllers
{
    [TestClass]
    public class TestClientController
    {
        // mocks
        private IDatabaseFacade dbFacadeMock;
        private IDatabaseFacadeFactory dbFacadeFactoryMock;

        private ClientController clientController;

        [TestInitialize]
        public void Setup()
        {
            dbFacadeMock = MockRepository.GenerateMock<IDatabaseFacade>();
            dbFacadeFactoryMock = MockRepository.GenerateMock<IDatabaseFacadeFactory>();
            dbFacadeFactoryMock.Stub(dbFacadeFactory => dbFacadeFactory.Create())
                .Return(dbFacadeMock);

            clientController = new ClientController(dbFacadeFactoryMock);
        }

        [TestMethod]
        [TestCategory("ClientController")]
        public void ClientDetail_NonExistingClient_ShouldReturnNotFound()
        {
            // arrange
            var userId = Guid.NewGuid().ToString();
            var userName = "user";

            var employee = new SiteEngineer()
            {
                Id = 1,
                UserId = userId,
                Name = "Employee",
                District = District.SYDNEY,
                MaxDuration = 10,
                MaxCosts = 500
            };

            var intervention = new Intervention()
            {
                Id = 1,
                Duration = 2,
                Costs = 100,
                Notes = "Note",
                Created = DateTime.Now
            };

            var interventionType = new InterventionType()
            {
                Id = 1,
                Name = "InterventionType",
                Duration = 2,
                Costs = 100
            };

            var client = new Client()
            {
                Id = 1,
                Name = "Client",
                District = District.SYDNEY,
                ClientType = ClientType.PERSON,
                Location = "Location"
            };

            // set model object references
            employee.Interventions = new List<Intervention>() { intervention };
            intervention.Client = client;
            intervention.Employee = employee;
            intervention.InterventionType = interventionType;
            interventionType.Interventions = new List<Intervention>() { intervention };
            client.Interventions = new List<Intervention>() { intervention };

            // configure mocks
            clientController.ControllerContext = TestHelper.MockControllerContext(clientController).WithAuthenticatedUser(userId, userName);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetAll<Intervention>())
                .Return(null);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetById<Client>(client.Id))
                .Return(null);

            // act
            var result = (HttpStatusCodeResult)clientController.ClientDetail(intervention.Id);

            // assert
            Assert.AreEqual((int)HttpStatusCode.NotFound, result.StatusCode);
        }

        [TestMethod]
        [TestCategory("ClientController")]
        public void ClientDetail_ExistingClient_ShouldReturnView()
        {
            // arrange
            var userId = Guid.NewGuid().ToString();
            var userName = "user";

            var employee = new SiteEngineer()
            {
                Id = 1,
                UserId = userId,
                Name = "Employee",
                District = District.SYDNEY,
                MaxDuration = 10,
                MaxCosts = 500
            };

            var intervention = new Intervention()
            {
                Id = 1,
                Duration = 2,
                Costs = 100,
                Notes = "Note",
                Created = DateTime.Now
            };

            var interventionType = new InterventionType()
            {
                Id = 1,
                Name = "InterventionType",
                Duration = 2,
                Costs = 100
            };

            var client = new Client()
            {
                Id = 1,
                Name = "Client",
                District = District.SYDNEY,
                ClientType = ClientType.PERSON,
                Location = "Location"
            };

            // set model object references
            employee.Interventions = new List<Intervention>() { intervention };
            intervention.Client = client;
            intervention.Employee = employee;
            intervention.InterventionType = interventionType;
            interventionType.Interventions = new List<Intervention>() { intervention };
            client.Interventions = new List<Intervention>() { intervention };

            // configure mocks
            clientController.ControllerContext = TestHelper.MockControllerContext(clientController).WithAuthenticatedUser(userId, userName);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetEmployeeByUserId(userId))
                .Return(employee);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetById<Client>(client.Id))
                .Return(client);

            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetAll<Intervention>())
                .Return(new List<Intervention>());

            // act
            var result = (ViewResult)clientController.ClientDetail(client.Id);
            var viewModel = (ClientDetailViewModel)result.Model;

            // assert
            Assert.AreEqual(client.Name, viewModel.Client.Name);
        }

        [TestMethod]
        [TestCategory("ClientController")]
        public void NewClient_SaveClient_ShouldInvokeSaveOrUpdate()
        {
            // arrange
            var userId = Guid.NewGuid().ToString();
            var userName = "user";

            var employee = new SiteEngineer()
            {
                Id = 1,
                UserId = userId,
                Name = "Employee",
                District = District.SYDNEY,
                MaxDuration = 10,
                MaxCosts = 500
            };

            var intervention = new Intervention()
            {
                Id = 1,
                Duration = 2,
                Costs = 100,
                Notes = "Note",
                Created = DateTime.Now
            };

            var interventionType = new InterventionType()
            {
                Id = 1,
                Name = "InterventionType",
                Duration = 2,
                Costs = 100
            };

            var client = new Client()
            {
                Id = 1,
                Name = "Client",
                District = District.SYDNEY,
                ClientType = ClientType.PERSON,
                Location = "Location"
            };

            // set model object references
            employee.Interventions = new List<Intervention>() { intervention };
            intervention.Client = client;
            intervention.Employee = employee;
            intervention.InterventionType = interventionType;
            interventionType.Interventions = new List<Intervention>() { intervention };
            client.Interventions = new List<Intervention>() { intervention };

            // configure mocks
            clientController.ControllerContext = TestHelper.MockControllerContext(clientController).WithAuthenticatedUser(userId, userName);
            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetEmployeeByUserId(userId))
                .Return(employee);
            dbFacadeMock.Expect(dbFacadeMock => dbFacadeMock.SaveOrUpdate(client));
            // act
            NewClientViewModel viewModel = new NewClientViewModel()
            {
                Client = client
            };
            var result = (RedirectToRouteResult)clientController.NewClient(viewModel);

            // assert
            dbFacadeMock.VerifyAllExpectations();
        }
        
        [TestMethod]
        [TestCategory("ClientController")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NewIntervention_InvalidIntervention_ShouldThrowException()
        {
            // arrange
            var userId = Guid.NewGuid().ToString();
            var userName = "user";

            var employee = new SiteEngineer()
            {
                Id = 1,
                UserId = userId,
                Name = "Employee",
                District = District.SYDNEY,
                MaxDuration = 10,
                MaxCosts = 500
            };

            var client = new Client()
            {
                Id = 1,
                Name = "Client",
                District = District.SYDNEY,
                ClientType = ClientType.PERSON,
                Location = "Location"
            };

            var interventionType = new InterventionType()
            {
                Id = 1,
                Name = "InterventionType",
                Duration = 2,
                Costs = 100,
            };

            var intervention = new Intervention()
            {
                Id = 1,
                Duration = 2,
                Costs = 100,
                Notes = "Note",
                Created = DateTime.Now,
                EmployeeId = employee.Id,
                ClientId = client.Id,
                InterventionTypeId = interventionType.Id
            };

            // set model object references
            employee.Interventions = new List<Intervention>() { intervention };
            intervention.Client = client;
            intervention.Employee = employee;
            intervention.InterventionType = interventionType;
            interventionType.Interventions = new List<Intervention>() { intervention };
            client.Interventions = new List<Intervention>() { intervention };

            // configure mocks
            clientController.ControllerContext = TestHelper.MockControllerContext(clientController).WithAuthenticatedUser(userId, userName);
            dbFacadeMock.Stub(dbFacadeMock => dbFacadeMock.GetEmployeeByUserId(userId))
                .Return(employee);
            dbFacadeMock.Expect(dbFacadeMock => dbFacadeMock.SaveOrUpdate(intervention));
            // act
             NewInterventionViewModel viewModel = new NewInterventionViewModel()
            {
                ClientName = client.Name,
                Cost = intervention.Costs,
                //no duration
                CurrentDate = intervention.Created,
                SiteEngineer = intervention.Employee.Name,
                SelectedValue = intervention.InterventionTypeId
            };
            var result = (RedirectToRouteResult)clientController.NewIntervention(client.Id, false, viewModel);

            // assert
            dbFacadeMock.VerifyAllExpectations();
        }
    }
}
