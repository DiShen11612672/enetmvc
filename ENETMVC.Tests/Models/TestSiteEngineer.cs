﻿using ENETMVC.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ENETMVC.IMSTests
{
    [TestClass]
    public class TestSiteEngineer
    {
        // test object
        SiteEngineer siteEngineer;

        [TestInitialize]
        public void Setup()
        {
            siteEngineer = new SiteEngineer();
        }

        [TestMethod]
        [TestCategory("SiteEngineer")]
        public void SiteEngineer_InitialMaxDuration()
        {
            Assert.AreEqual(0, siteEngineer.MaxDuration);
        }

        [TestMethod]
        [TestCategory("SiteEngineer")]
        public void SiteEngineer_InitialMaxCost()
        {
            Assert.AreEqual(0, siteEngineer.MaxCosts);
        }

        [TestMethod]
        [TestCategory("SiteEngineer")]
        public void SiteEngineer_SetDistrict()
        {
            siteEngineer.District = District.RURAL_INDONESIA;
            Assert.AreEqual(District.RURAL_INDONESIA, siteEngineer.District);
        }

        [TestMethod]
        [TestCategory("SiteEngineer")]
        public void SiteEngineer_SetMaxCost()
        {
            siteEngineer.MaxCosts = 99999999;
            Assert.AreEqual(99999999, siteEngineer.MaxCosts);
        }

        [TestMethod]
        [TestCategory("SiteEngineer")]
        public void SiteEngineer_SetMaxDuration()
        {
            siteEngineer.MaxDuration = 555;
            Assert.AreEqual(555, siteEngineer.MaxDuration);
        }
    }
}
