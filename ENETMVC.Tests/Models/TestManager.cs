﻿using ENETMVC.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ENETMVC.IMSTests
{
    [TestClass]
    public class TestManager
    {
        // Test object.
        private Manager manager;

        [TestInitialize]
        public void Setup()
        {
            manager = new Manager(999, "Ethel McDougal", "ethelmcdougal", District.RURAL_NSW, 123456789, 987654321);
        }

        [TestMethod]
        [TestCategory("Manager")]
        public void Manager_OnInitialisation_IsEthelMcDougal()
        {
            Assert.AreEqual("Ethel McDougal", manager.Name);
        }

        [TestMethod]
        [TestCategory("Manager")]
        public void Manager_OnInitialisation_Is_ethelmcdougal()
        {
            Assert.AreEqual("ethelmcdougal", manager.UserId);
        }

        [TestMethod]
        [TestCategory("Manager")]
        public void Manager_OnInitialisation_IsRuralNewSouthWales()
        {
            Assert.AreEqual(District.RURAL_NSW, manager.District);
        }

        [TestMethod]
        [TestCategory("Manager")]
        public void Manager_OnInitialisation_MaxDuration_Is123456789Point98()
        {
            Assert.AreEqual(123456789, manager.MaxDuration);
        }

        [TestMethod]
        [TestCategory("Manager")]
        public void Manager_OnInitialisation_MaxCosts_Is987654321Point12()
        {
            Assert.AreEqual(987654321, manager.MaxCosts);
        }

        [TestMethod]
        [TestCategory("Manager")]
        public void Manager_OnInitialisation_NoProposedInterventions()
        {
            Assert.AreEqual(0, manager.ProposedInterventions.Count);
        }

        [TestMethod]
        [TestCategory("Manager")]
        public void Manager_OnInitialisation_NoPastInterventions()
        {
            Assert.AreEqual(0, manager.PastInterventions.Count);
        }

        [TestMethod]
        [TestCategory("Manager")]
        public void Manager_AddProposedInterventions_InterventionDetails_Retrievable()
        {
            // SiteEngineer object to add to intervention.
            var siteEngineer = new SiteEngineer();
            siteEngineer.District = manager.District;
            siteEngineer.Name = "Bob Norman";
            siteEngineer.MaxCosts = 99;
            siteEngineer.MaxDuration = 99;

            // Client object to add to intervention.
            var client = new Client()
            {
                Name = "Fred Nerk",
                Location = "123 fourth street, fivetown",
                District = manager.District
            };

            // InterventionType object to add to intervention.
            var interventionType = new InterventionType();
            interventionType.Name = "new toilet";
            interventionType.Duration = 123;
            interventionType.Costs = 321;

            // Intervention object to add to manager's list of proposed interventions.
            var intervention = new Intervention(siteEngineer, client, interventionType);

            // Add new intervention to manager's list of proposed interventions.
            manager.ProposedInterventions.Add(intervention);

            // There should be one proposed intervention in the manager's proposed list.
            Assert.AreEqual(1, manager.ProposedInterventions.Count);

            // The item in the manager's proposed list should not be null.
            Assert.IsNotNull(manager.ProposedInterventions.Find(item => item.Client.Name == "Fred Nerk"));
        }

        [TestMethod]
        [TestCategory("Manager")]
        public void Manager_AddPastInterventions_InterventionDetails_Retrievable()
        {
            // SiteEngineer object to add to intervention.
            var siteEngineer = new SiteEngineer();
            siteEngineer.District = District.RURAL_INDONESIA;
            siteEngineer.Name = "Morgan Freeman";
            siteEngineer.MaxCosts = 88;
            siteEngineer.MaxDuration = 88;

            // Client object to add to intervention.
            var client = new Client()
            {
                Name = "Mary Scott",
                District = District.RURAL_INDONESIA,
                Location = "456 seventh street, ninetown"
            };

            // InterventionType object to add to intervention.
            var interventionType = new InterventionType();
            interventionType.Name = "new lean-to";
            interventionType.Duration = 456;
            interventionType.Costs = 789;

            // Intervention object to add to manager's list of proposed interventions.
            var intervention = new Intervention(siteEngineer, client, interventionType, State.COMPLETE);

            // Add new intervention to manager's list of proposed interventions.
            manager.PastInterventions.Add(intervention);

            // There should be one proposed intervention in the manager's proposed list.
            Assert.AreEqual(1, manager.PastInterventions.Count);

            // The item in the manager's proposed list should not be null.
            Assert.IsNotNull(manager.PastInterventions.Find(item => item.Client.Name == "Mary Scott"));
        }
    }
}