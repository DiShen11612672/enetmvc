﻿using ENETMVC.Database.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ENETMVC.IMSTests
{
    [TestClass]
    public class TestClient
    {
        // test object
        private Client client;

        [TestInitialize]
        public void Setup()
        {
            client = new Client()
            {
                Name = "Fred Nerk",
                Location = "123 fourth street fivetown",
                District = District.RURAL_NSW
            };
        }

        [TestMethod]
        [TestCategory("Client")]
        public void Client_OnInitialisation_IsFredNerk()
        {
            Assert.AreEqual("Fred Nerk", client.Name);
        }

        [TestMethod]
        [TestCategory("Client")]
        public void Client_OnInitialisation_Is123FourthStreet()
        {
            Assert.AreEqual("123 fourth street fivetown", client.Location);
        }

        [TestMethod]
        [TestCategory("Client")]
        public void Client_OnInitialisation_IsRuralNewSouthWales()
        {
            Assert.AreEqual(District.RURAL_NSW, client.District);
        }

        /*
        [TestMethod]
        [TestCategory("Client")]
        [ExpectedException(typeof(ArgumentException), "Property must contain "
            + "text other than null or whitespace.")]
        public void Client_EmptyNameInConstructor()
        {
            Client c = new Client("", "321 9th street ultimo", District.SYDNEY);
        } // reference - https://msdn.microsoft.com/en-us/library/ms379625(v=vs.80).aspx

        [TestMethod]
        [TestCategory("Client")]
        [ExpectedException(typeof(ArgumentException), "Property must contain "
            + "text other than null or whitespace.")]
        public void Client_WhitespaceNameInConstructor()
        {
            Client c = new Client("     ", "321 9th street ultimo", District.SYDNEY);
        } // reference - https://msdn.microsoft.com/en-us/library/ms379625(v=vs.80).aspx

        [TestMethod]
        [TestCategory("Client")]
        [ExpectedException(typeof(ArgumentException), "Property must contain "
            + "text other than null or whitespace.")]
        public void Client_NullLocationInConstructor()
        {
            Client c = new Client("Sam Strangelove", null, District.RURAL_NSW);
        } // reference - https://msdn.microsoft.com/en-us/library/ms379625(v=vs.80).aspx

        [TestMethod]
        [TestCategory("Client")]
        [ExpectedException(typeof(ArgumentException), "Property must contain "
            + "text other than null or whitespace.")]
        public void Client_EmptyLocationInConstructor_Error()
        {
            Client c = new Client("Sam Strangelove", "", District.RURAL_NSW);
        } // reference - https://msdn.microsoft.com/en-us/library/ms379625(v=vs.80).aspx

        [TestMethod]
        [TestCategory("Client")]
        [ExpectedException(typeof(ArgumentException), "Property must contain "
            + "text other than null or whitespace.")]
        public void Client_WhitespaceLocationInConstructor()
        {
            Client c = new Client("Sam Strangelove", "     ", District.RURAL_NSW);
        } // reference - https://msdn.microsoft.com/en-us/library/ms379625(v=vs.80).aspx
        */
    }
}
