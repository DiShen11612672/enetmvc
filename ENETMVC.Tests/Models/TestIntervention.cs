﻿using ENETMVC.Database.Models;
using ENETMVC.Database.Models.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ENETMVC.Tests.Models
{
    [TestClass]
    public class TestIntervention
    {
        private District district;
        private SiteEngineer siteEngineer;
        private Manager manager;
        private Client client;
        private InterventionType interventionType;

        // test object
        private Intervention intervention;

        [TestInitialize]
        public void Setup()
        {
            district = District.RURAL_INDONESIA;

            siteEngineer = new SiteEngineer()
            {
                Id = 1,
                District = district,
                MaxCosts = 200,
                MaxDuration = 10
            };

            manager = new Manager()
            {
                Id = 3,
                District = district,
                MaxCosts = 200,
                MaxDuration = 10
            };

            client = new Client()
            {
                Name = "name",
                Location = "location",
                District = district
            };

            interventionType = new InterventionType()
            {
                Id = 1,
                Costs = 120,
                Duration = 5
            };
        }

        [TestMethod]
        [TestCategory("Intervention")]
        public void Intervention_InitialisationWithProperParameters()
        {
            intervention = new Intervention(siteEngineer, client, interventionType);

            // Assert
            Assert.AreEqual(intervention.Employee, siteEngineer);
            Assert.AreEqual(intervention.Client, client);
            Assert.AreEqual(intervention.InterventionType, interventionType);
            Assert.AreEqual(intervention.Duration, interventionType.Duration);
            Assert.AreEqual(intervention.Costs, interventionType.Costs);
            Assert.IsNotNull(intervention.Created);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Intervention_InitialisationWithInvalidParameters_Error1()
        {
            // Act
            intervention = new Intervention(null, client, interventionType);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Intervention_InitialisationWithInvalidParameters_Error2()
        {
            // Act
            intervention = new Intervention(siteEngineer, null, interventionType);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Intervention_InitialisationWithInvalidParameters_Error3()
        {
            // Act
            intervention = new Intervention(siteEngineer, client, null);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        public void ProposeToApprove_SiteEngineerWithinQuota_ShouldChangeStateToApprove()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 2
            };

            // Act
            intervention.Approve(siteEngineer);

            // Assert
            Assert.AreEqual(State.APPROVE, intervention.State);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        public void ProposeToApprove_ManagerWithinQuota_ShouldChangeStateToApprove()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 2
            };

            // Act
            intervention.Approve(manager);

            // Assert
            Assert.AreEqual(State.APPROVE, intervention.State);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToApprove_SiteEngineerExceedingMaxDuration_Error()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 15
            };

            // Act
            intervention.Approve(siteEngineer);
        }
        
        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToApprove_ManagerExceedingMaxDuration_Error()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 15
            };

            // Act
            intervention.Approve(manager);
        }
        
        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToApprove_SiteEngineerExceedingMaxCosts_Error()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 230,
                Duration = 5
            };

            // Act
            intervention.Approve(siteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToApprove_ManagerExceedingMaxCosts_Error()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 230,
                Duration = 5
            };

            // Act
            intervention.Approve(manager);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToApprove_OtherSiteEngineer_Error()
        {
            // Arrange
            var otherSiteEngineer = new SiteEngineer()
            {
                Id = 2,
                District = District.RURAL_INDONESIA,
                MaxCosts = 500,
                MaxDuration = 30
            };

            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 3
            };

            // Act
            intervention.Approve(otherSiteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToApprove_ManagerWithOtherDistrict_Error()
        {
            // Arrange
            manager.District = District.SYDNEY;

            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 3
            };

            // Act
            intervention.Approve(manager);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToApprove_SiteEngineerExceedingInterventionTypeCosts_Error()
        {
            // Arrange
            interventionType.Costs = 500;
            interventionType.Duration = 4;

            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 120,
                Duration = 2
            };

            // Act
            intervention.Approve(siteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToApprove_ManagerExceedingInterventionTypeCosts_Error()
        {
            // Arrange
            interventionType.Costs = 500;
            interventionType.Duration = 4;

            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 120,
                Duration = 2
            };

            // Act
            intervention.Approve(manager);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToApprove_SiteEngineerExceedingInterventionTypeDuration_Error()
        {
            // Arrange
            interventionType.Costs = 100;
            interventionType.Duration = 30;

            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 4
            };

            // Act
            intervention.Approve(siteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToApprove_ManagerExceedingInterventionTypeDuration_Error()
        {
            // Arrange
            interventionType.Costs = 100;
            interventionType.Duration = 30;

            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 4
            };

            // Act
            intervention.Approve(manager);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        public void ProposeToCancel_SameSiteEngineer_ShouldChangeStateToCancel()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 3
            };

            // Act
            intervention.Cancel(siteEngineer);

            // Assert
            Assert.AreEqual(State.CANCEL, intervention.State);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToCancel_OtherSiteEngineer_Error()
        {
            // Arrange
            var otherSiteEngineer = new SiteEngineer()
            {
                Id = 2,
                District = District.RURAL_INDONESIA,
                MaxCosts = 500,
                MaxDuration = 30
            };

            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 3
            };

            // Act
            intervention.Cancel(otherSiteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        public void ProposeToCancel_ManagerWithSameDistrict_ShouldChangeStateToCancel()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 3
            };

            // Act
            intervention.Cancel(manager);

            // Assert
            Assert.AreEqual(State.CANCEL, intervention.State);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToCancel_ManagerWithOtherDistrict_Error()
        {
            // Arrange
            manager.District = District.SYDNEY;

            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 3
            };

            // Act
            intervention.Cancel(manager);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ProposeToComplete_Error()
        {
            // Arrange        
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 3
            };

            // Act
            intervention.Complete(siteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ApproveToApprove_Error()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 2
            };
            intervention.Approve(siteEngineer);

            // Act
            intervention.Approve(siteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        public void ApproveToCancel_SameSiteEngineer_ShouldChangeStateToCancel()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 2
            };
            intervention.Approve(siteEngineer);

            // Act
            intervention.Cancel(siteEngineer);

            // Assert
            Assert.AreEqual(State.CANCEL, intervention.State);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ApproveToCancel_OtherSiteEngineer_Error()
        {
            // Arrange
            var otherSiteEngineer = new SiteEngineer()
            {
                Id = 2,
                District = District.RURAL_INDONESIA,
                MaxCosts = 500,
                MaxDuration = 30
            };

            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 120,
                Duration = 5
            };
            intervention.Approve(siteEngineer);

            // Act
            intervention.Cancel(otherSiteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        public void ApproveToComplete_SameSiteEngineer_ShouldChangeStateToComplete()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 2
            };
            intervention.Approve(siteEngineer);

            // Act
            intervention.Complete(siteEngineer);

            // Assert
            Assert.AreEqual(State.COMPLETE, intervention.State);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void ApproveToComplete_OtherSiteEngineer_Error()
        {
            // Arrange
            var otherSiteEngineer = new SiteEngineer()
            {
                Id = 2,
                District = District.RURAL_INDONESIA,
                MaxCosts = 500,
                MaxDuration = 30
            };

            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 120,
                Duration = 5
            };
            intervention.Approve(siteEngineer);

            // Act
            intervention.Complete(otherSiteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void CancelToCancel_Error()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 2
            };
            intervention.Cancel(siteEngineer);

            // Act
            intervention.Cancel(siteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void CancelToApprove_Error()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 2
            };
            intervention.Cancel(siteEngineer);

            // Act
            intervention.Approve(siteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void CancelToComplete_Error()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 2
            };
            intervention.Cancel(siteEngineer);

            // Act
            intervention.Complete(siteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void CompleteToComplete_Error()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 2
            };
            intervention.Approve(siteEngineer);
            intervention.Complete(siteEngineer);

            // Act
            intervention.Complete(siteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void CompleteToApprove_Error()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 2
            };
            intervention.Approve(siteEngineer);
            intervention.Complete(siteEngineer);

            // Act
            intervention.Approve(siteEngineer);
        }

        [TestMethod]
        [TestCategory("Intervention")]
        [ExpectedException(typeof(InterventionStateChangeException))]
        public void CompleteToCancel_Error()
        {
            // Arrange
            intervention = new Intervention(siteEngineer, client, interventionType)
            {
                Costs = 100,
                Duration = 2
            };
            intervention.Approve(siteEngineer);
            intervention.Complete(siteEngineer);

            // Act
            intervention.Cancel(siteEngineer);
        }
    }
}
